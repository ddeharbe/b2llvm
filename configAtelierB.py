#!/usr/bin/python2.7
import os
import sys

etool = '''<externalTool category="component"   name="b2llvm_comp" label="&amp;B2LLVM compilation "   shortcut="Ctrl+G"  >
<toolParameter name="editor" type="tool" configure="yes"
default="'''+sys.executable+'''"/>
    <command>${editor}</command>
    <param>CurrentPathb2llvm.py</param>
    <param>-a</param>
    '''

def installExtension(path):
    global etool
    etoolend = '''
    <param>${componentName}</param>
    <param>${componentName}.llvm</param>
    <param>${projectBdp}</param>
    <param>project.xml</param>
    <param>${projectTrad}</param>
</externalTool>'''

    res= etool + '    <param>-m</param>    <param>comp</param>'

    res = res.replace("CurrentPathb2llvm.py", os.path.realpath(__file__).replace("configAtelierB","b2llvm")) + etoolend
    f = open(path+os.sep+'b2llvm.etool','w')
    f.write(res)
    f.close()

    #some changes to create the option proj
    res= etool + '    <param>-m</param>    <param>proj</param>'
    res = res.replace(';B2LLVM compilation',';B2LLVM link-edition')
    res = res.replace('Ctrl+G','Ctrl+L')
    res = res.replace('b2llvm_comp','b2llvm_proj')
    res_end = etoolend.replace('<param>${componentName}.llvm</param>','<param>init_${componentName}.llvm</param>')
    res = res.replace("CurrentPathb2llvm.py", os.path.realpath(__file__).replace("configAtelierB","b2llvm")) + res_end
    f = open(path+os.sep+'b2llvm_proj.etool','w')
    f.write(res)
    f.close()

def question(msg,opt,optconc):
    global etool
    ans = raw_input(msg)
    valid=False
    for i in range(len(opt)):
        if ans in opt[i]:
            etool= etool + optconc[i]
            valid=True
            break
    if not(valid):
        etool= etool + optconc[0]
        print("Invalid option and the used option was:"+opt[0])


opt =['yes','no']
optcon = ['    <param>-t</param>','']
question('Do you want to enable emission of references to B source in LLVM code (yes/no)?',opt,optcon)

opt =['yes','no']
optcon = ['    <param>-p</param>','']
question('Do you want to enable emission of LLVM functions that print the state of the components (yes/no)?',opt,optcon)

opt =['yes','no']
optcon = ['    <param>-v</param>','']
question('Do you want to enable emission of outputs some information about compilation while running (yes/no)?',opt,optcon)

path = raw_input("Please, type the path of Atelier B extensions, (for example  '/Applications/AtelierB.app/AB/extensions'):")
installExtension(path)
exit()

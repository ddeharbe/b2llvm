#include <stdio.h>
#include "Sort.llvm.h"

Sort$state$ self;

void printv(){
    int i;
    for(i=0;i<10;i++){
        printf("%i ",self.vector[i]);
    }
    printf("\n");
    
}
int main(){
    
    Sort$init$(&self);
    printv();
    Sort$op_sort(&self);
    printv();
    
    Sort$ascendingB(&self);
    
    printv();
      
    return 0;
}

#ifndef _Sort_h
#define _Sort_h

#include <stdint.h>
#include <stdbool.h>

extern void Sort$init$();
extern void Sort$op_sort();
extern void Sort$swap();
extern void Sort$ascendingB();
#endif /* _Sort_h */

#ifndef _Sortllvm_h
#define _Sortllvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t mvector_i[2]; int32_t vector[10]; bool descending_sort; }   Sort$state$;
typedef Sort$state$ * Sort$ref$;
extern void Sort$init$(Sort$ref$ self);
extern void Sort$op_sort(Sort$ref$ self);
extern void Sort$swap(Sort$ref$ self);
extern void Sort$ascendingB(Sort$ref$ self);
#endif /* _Sortllvm_h */

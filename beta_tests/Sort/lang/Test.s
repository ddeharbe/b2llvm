	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_printv
	.align	4, 0x90
_printv:                                ## @printv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$16, %rsp
Ltmp3:
	.cfi_offset %rbx, -32
Ltmp4:
	.cfi_offset %r14, -24
	movl	$0, -20(%rbp)
	movq	_self@GOTPCREL(%rip), %r14
	leaq	L_.str(%rip), %rbx
	jmp	LBB0_1
	.align	4, 0x90
LBB0_2:                                 ##   in Loop: Header=BB0_1 Depth=1
	movslq	-20(%rbp), %rax
	movl	8(%r14,%rax,4), %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	_printf
	incl	-20(%rbp)
LBB0_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$9, -20(%rbp)
	jle	LBB0_2
## BB#3:
	leaq	L_.str1(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp5:
	.cfi_def_cfa_offset 16
Ltmp6:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp7:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp8:
	.cfi_offset %rbx, -24
	movl	$0, -12(%rbp)
	movq	_self@GOTPCREL(%rip), %rbx
	movq	%rbx, %rdi
	callq	_Sort$init$
	callq	_printv
	movq	%rbx, %rdi
	callq	_Sort$op_sort
	callq	_printv
	movq	%rbx, %rdi
	callq	_Sort$ascendingB
	callq	_printv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"%i "

	.comm	_self,52,2              ## @self
L_.str1:                                ## @.str1
	.asciz	"\n"


.subsections_via_symbols

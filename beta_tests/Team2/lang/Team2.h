#ifndef _Team2_h
#define _Team2_h

#include <stdint.h>
#include <stdbool.h>

extern void Team2$init$();
extern void Team2$substitute();
extern void Team2$in_team();
#endif /* _Team2_h */

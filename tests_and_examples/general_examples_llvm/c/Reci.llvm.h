#ifndef __Users_valerio_Dropbox_Doutorado_workspace_b2llvm_tests_and_examples_general_examples_llvm_Recillvm_h
#define __Users_valerio_Dropbox_Doutorado_workspace_b2llvm_tests_and_examples_general_examples_llvm_Recillvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {struct { int32_t f1, int32_t f2, bool f3 } }   Rec$state$;
typedef Rec$state$ * Rec$ref$;
extern void Rec$init$(Rec$ref$ self);
extern void Rec$test(Rec$ref$ self);
extern void Rec$withdraw(Rec$ref$ self, int32_t amt);
extern void Rec$similar_withdraw(Rec$ref$ self, int32_t amt);
#endif /* __Users_valerio_Dropbox_Doutorado_workspace_b2llvm_tests_and_examples_general_examples_llvm_Recillvm_h */

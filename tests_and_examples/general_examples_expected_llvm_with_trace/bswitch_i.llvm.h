#ifndef __tests_and_examples_general_examples_llvm_bswitch_illvm_h
#define __tests_and_examples_general_examples_llvm_bswitch_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t v1; int32_t v2; }   bswitch$state$;
typedef bswitch$state$ * bswitch$ref$;
extern void bswitch$init$(bswitch$ref$ self);
extern void bswitch$step(bswitch$ref$ self);
#endif /* __tests_and_examples_general_examples_llvm_bswitch_illvm_h */

#ifndef __tests_and_examples_general_examples_llvm_ATM_illvm_h
#define __tests_and_examples_general_examples_llvm_ATM_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {struct { int32_t idac; int32_t balance; };; }   ATM$state$;
typedef ATM$state$ * ATM$ref$;
extern void ATM$init$(ATM$ref$ self);
extern void ATM$deposit(ATM$ref$ self, int32_t mm);
extern void ATM$withdraw(ATM$ref$ self, int32_t mm);
extern void ATM$balance(ATM$ref$ self, int32_t* bb);
#endif /* __tests_and_examples_general_examples_llvm_ATM_illvm_h */

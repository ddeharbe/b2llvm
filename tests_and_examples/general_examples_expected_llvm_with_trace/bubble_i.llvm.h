#ifndef __tests_and_examples_general_examples_llvm_bubble_illvm_h
#define __tests_and_examples_general_examples_llvm_bubble_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t vec1[100]; int32_t sort1; }   bubble$state$;
typedef bubble$state$ * bubble$ref$;
extern void bubble$init$(bubble$ref$ self);
extern void bubble$op_sort(bubble$ref$ self);
#endif /* __tests_and_examples_general_examples_llvm_bubble_illvm_h */

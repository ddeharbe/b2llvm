#ifndef __tests_and_examples_general_examples_llvm_mult_illvm_h
#define __tests_and_examples_general_examples_llvm_mult_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t op1; int32_t op2; int32_t res; bool ok; }   mult$state$;
typedef mult$state$ * mult$ref$;
extern void mult$init$(mult$ref$ self);
extern void mult$inc1(mult$ref$ self);
extern void mult$dec1(mult$ref$ self);
extern void mult$inc2(mult$ref$ self);
extern void mult$dec2(mult$ref$ self);
extern void mult$calc(mult$ref$ self);
#endif /* __tests_and_examples_general_examples_llvm_mult_illvm_h */

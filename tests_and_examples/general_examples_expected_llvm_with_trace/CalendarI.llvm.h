#ifndef __tests_and_examples_general_examples_llvm_CalendarIllvm_h
#define __tests_and_examples_general_examples_llvm_CalendarIllvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {i4 mmi; }   Calendar$state$;
typedef Calendar$state$ * Calendar$ref$;
extern void Calendar$init$(Calendar$ref$ self);
extern void Calendar$month(Calendar$ref$ self, int32_t nn);
#endif /* __tests_and_examples_general_examples_llvm_CalendarIllvm_h */

#ifndef __tests_and_examples_general_examples_llvm_MOD_SizeCounterIllvm_h
#define __tests_and_examples_general_examples_llvm_MOD_SizeCounterIllvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t sze; }   MOD_SizeCounter$state$;
typedef MOD_SizeCounter$state$ * MOD_SizeCounter$ref$;
extern void MOD_SizeCounter$init$(MOD_SizeCounter$ref$ self);
extern void MOD_SizeCounter$szeinc(MOD_SizeCounter$ref$ self);
extern void MOD_SizeCounter$szedec(MOD_SizeCounter$ref$ self);
extern void MOD_SizeCounter$szeget(MOD_SizeCounter$ref$ self, int32_t* ss);
#endif /* __tests_and_examples_general_examples_llvm_MOD_SizeCounterIllvm_h */

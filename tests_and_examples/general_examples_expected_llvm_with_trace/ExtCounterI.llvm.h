#ifndef __tests_and_examples_general_examples_llvm_ExtCounterIllvm_h
#define __tests_and_examples_general_examples_llvm_ExtCounterIllvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t value; bool overflow_error; bool underflow_error; }   ExtCounter$state$;
typedef ExtCounter$state$ * ExtCounter$ref$;
extern void ExtCounter$init$(ExtCounter$ref$ self);
extern void ExtCounter$zero(ExtCounter$ref$ self);
extern void ExtCounter$inc(ExtCounter$ref$ self);
extern void ExtCounter$dec(ExtCounter$ref$ self);
extern void ExtCounter$get(ExtCounter$ref$ self, int32_t* res);
extern void ExtCounter$doTest(ExtCounter$ref$ self);
#endif /* __tests_and_examples_general_examples_llvm_ExtCounterIllvm_h */

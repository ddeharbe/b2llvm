#ifndef __tests_and_examples_general_examples_llvm_Recillvm_h
#define __tests_and_examples_general_examples_llvm_Recillvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {struct { int32_t f1; int32_t f2; bool f3; };; struct { int32_t z1; int32_t z2; bool z3; };; }   Rec$state$;
typedef Rec$state$ * Rec$ref$;
extern void Rec$init$(Rec$ref$ self);
extern void Rec$test(Rec$ref$ self);
extern void Rec$withdraw(Rec$ref$ self, int32_t amt);
extern void Rec$similar_withdraw(Rec$ref$ self, int32_t amt);
#endif /* __tests_and_examples_general_examples_llvm_Recillvm_h */

#ifndef __tests_and_examples_general_examples_llvm_PrimeImpllvm_h
#define __tests_and_examples_general_examples_llvm_PrimeImpllvm_h

#include <stdint.h>
#include <stdbool.h>

extern void Prime$init$();
extern void Prime$IsPrime(int32_t xx, bool* rr);
#endif /* __tests_and_examples_general_examples_llvm_PrimeImpllvm_h */

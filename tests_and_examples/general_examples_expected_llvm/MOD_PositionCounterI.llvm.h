#ifndef __tests_and_examples_general_examples_llvm_MOD_PositionCounterIllvm_h
#define __tests_and_examples_general_examples_llvm_MOD_PositionCounterIllvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t pos; }   MOD_PositionCounter$state$;
typedef MOD_PositionCounter$state$ * MOD_PositionCounter$ref$;
extern void MOD_PositionCounter$init$(MOD_PositionCounter$ref$ self);
extern void MOD_PositionCounter$posinc(MOD_PositionCounter$ref$ self);
extern void MOD_PositionCounter$posget(MOD_PositionCounter$ref$ self, int32_t* pp);
#endif /* __tests_and_examples_general_examples_llvm_MOD_PositionCounterIllvm_h */

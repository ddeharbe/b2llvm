#ifndef __tests_and_examples_general_examples_llvm_ExtCalculator2Illvm_h
#define __tests_and_examples_general_examples_llvm_ExtCalculator2Illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t op; int32_t aa; int32_t bb; int32_t rr; }   ExtCalculator2$state$;
typedef ExtCalculator2$state$ * ExtCalculator2$ref$;
extern void ExtCalculator2$init$(ExtCalculator2$ref$ self);
extern void ExtCalculator2$calculate(ExtCalculator2$ref$ self);
#endif /* __tests_and_examples_general_examples_llvm_ExtCalculator2Illvm_h */

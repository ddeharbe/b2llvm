#ifndef __tests_and_examples_general_examples_llvm_MOD_FifoIllvm_h
#define __tests_and_examples_general_examples_llvm_MOD_FifoIllvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t pos; }   MOD_PositionCounter$state$;
typedef MOD_PositionCounter$state$ * MOD_PositionCounter$ref$;
typedef struct {int32_t sze; }   MOD_SizeCounter$state$;
typedef MOD_SizeCounter$state$ * MOD_SizeCounter$ref$;
typedef struct {int32_t array[101]; }   MOD_Varray$state$;
typedef MOD_Varray$state$ * MOD_Varray$ref$;
typedef struct {MOD_Varray$ref$; MOD_SizeCounter$ref$; MOD_PositionCounter$ref$; }   MOD_Fifo$state$;
typedef MOD_Fifo$state$ * MOD_Fifo$ref$;
extern void MOD_Fifo$init$(MOD_Fifo$ref$ self, MOD_Varray$ref$ self, MOD_SizeCounter$ref$ self, MOD_PositionCounter$ref$ self);
extern void MOD_Fifo$input(MOD_Fifo$ref$ self, int32_t ee);
extern void MOD_Fifo$output(MOD_Fifo$ref$ self, int32_t* ee);
#endif /* __tests_and_examples_general_examples_llvm_MOD_FifoIllvm_h */

#ifndef __tests_and_examples_general_examples_llvm_Roomsillvm_h
#define __tests_and_examples_general_examples_llvm_Roomsillvm_h

#include <stdint.h>
#include <stdbool.h>

extern void Rooms$init$();
extern void Rooms$sizequery(int32_t rr, i1* ss);
extern void Rooms$pricequery(int32_t rr, int32_t* pp);
#endif /* __tests_and_examples_general_examples_llvm_Roomsillvm_h */

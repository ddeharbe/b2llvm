#ifndef __tests_and_examples_general_examples_llvm_timetracer_illvm_h
#define __tests_and_examples_general_examples_llvm_timetracer_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t value; bool error; }   counter$state$;
typedef counter$state$ * counter$ref$;
typedef struct {counter$ref$; counter$ref$; counter$ref$; bool is_running; bool overflow; }   timer$state$;
typedef timer$state$ * timer$ref$;
typedef struct {timer$ref$; timer$ref$; bool started; bool idle; }   timetracer$state$;
typedef timetracer$state$ * timetracer$ref$;
extern void timetracer$init$(timetracer$ref$ self, timer$ref$ self, timer$ref$ self, counter$ref$ self, counter$ref$ self, counter$ref$ self, counter$ref$ self, counter$ref$ self, counter$ref$ self);
extern void timetracer$enable(timetracer$ref$ self);
extern void timetracer$run(timetracer$ref$ self);
extern void timetracer$stop(timetracer$ref$ self);
extern void timetracer$disable(timetracer$ref$ self);
extern void timetracer$tick(timetracer$ref$ self);
extern void timetracer$peak(timetracer$ref$ self, int32_t* ts, int32_t* tr);
#endif /* __tests_and_examples_general_examples_llvm_timetracer_illvm_h */

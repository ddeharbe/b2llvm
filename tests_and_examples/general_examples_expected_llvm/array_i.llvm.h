#ifndef __tests_and_examples_general_examples_llvm_array_illvm_h
#define __tests_and_examples_general_examples_llvm_array_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t tmp_array[3][3]; int32_t v_array[3][3]; }   array$state$;
typedef array$state$ * array$ref$;
extern void array$init$(array$ref$ self);
extern void array$op1(array$ref$ self);
extern void array$op2(array$ref$ self);
extern void array$op3(array$ref$ self);
extern void array$op4(array$ref$ self);
#endif /* __tests_and_examples_general_examples_llvm_array_illvm_h */

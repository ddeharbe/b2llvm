#ifndef __tests_and_examples_general_examples_llvm_opbool_illvm_h
#define __tests_and_examples_general_examples_llvm_opbool_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {bool var1; }   opbool$state$;
typedef opbool$state$ * opbool$ref$;
extern void opbool$init$(opbool$ref$ self);
extern void opbool$set_t(opbool$ref$ self);
extern void opbool$set_f(opbool$ref$ self);
extern void opbool$op0(opbool$ref$ self);
extern void opbool$op1(opbool$ref$ self);
extern void opbool$op2(opbool$ref$ self);
extern void opbool$op3(opbool$ref$ self);
extern void opbool$op4(opbool$ref$ self);
extern void opbool$op5(opbool$ref$ self);
extern void opbool$op6(opbool$ref$ self);
extern void opbool$op7(opbool$ref$ self);
#endif /* __tests_and_examples_general_examples_llvm_opbool_illvm_h */

#ifndef __tests_and_examples_general_examples_llvm_Calculator_illvm_h
#define __tests_and_examples_general_examples_llvm_Calculator_illvm_h

#include <stdint.h>
#include <stdbool.h>

extern void Calculator$init$();
extern void Calculator$add(int32_t numberA, int32_t numberB, int32_t* result);
extern void Calculator$mult(int32_t numberA, int32_t numberB, int32_t* result);
extern void Calculator$div(int32_t numberA, int32_t numberB, int32_t* result);
extern void Calculator$modulo(int32_t numberA, int32_t numberB, int32_t* result);
extern void Calculator$pow(int32_t numberA, int32_t numberB, int32_t* result);
extern void Calculator$sub(int32_t numberA, int32_t numberB, int32_t* result);
#endif /* __tests_and_examples_general_examples_llvm_Calculator_illvm_h */

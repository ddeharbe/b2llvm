#ifndef __tests_and_examples_general_examples_llvm_enumeration_illvm_h
#define __tests_and_examples_general_examples_llvm_enumeration_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {i1 current; }   enumeration$state$;
typedef enumeration$state$ * enumeration$ref$;
extern void enumeration$init$(enumeration$ref$ self);
extern void enumeration$tick(enumeration$ref$ self);
extern void enumeration$read(enumeration$ref$ self, int32_t* res);
#endif /* __tests_and_examples_general_examples_llvm_enumeration_illvm_h */

#ifndef __tests_and_examples_general_examples_llvm_DivisionImplWrongConditionllvm_h
#define __tests_and_examples_general_examples_llvm_DivisionImplWrongConditionllvm_h

#include <stdint.h>
#include <stdbool.h>

extern void Division$init$();
extern void Division$div(int32_t aa, int32_t bb, int32_t* qq, int32_t* rr);
#endif /* __tests_and_examples_general_examples_llvm_DivisionImplWrongConditionllvm_h */

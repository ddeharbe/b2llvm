#include "bswitch.llvm.h"
#include <stdio.h>
const bool debug = false;
bswitch$state$ bswitch;

int main(){
    printf("%d  %d\n",bswitch.v1,bswitch.v2);
    bswitch$init$(&bswitch);
    
    printf("%d  %d\n",bswitch.v1,bswitch.v2); //0 0
    bswitch$step(&bswitch);
    
    printf("%d  %d\n",bswitch.v1,bswitch.v2); //1 0
    bswitch$step(&bswitch);
    
    printf("%d  %d\n",bswitch.v1,bswitch.v2); //2 0
    bswitch$step(&bswitch);
    
    printf("%d  %d\n",bswitch.v1,bswitch.v2); //4
    bswitch$step(&bswitch);
    
    printf("%d  %d\n",bswitch.v1,bswitch.v2);
    bswitch$step(&bswitch);
    printf("%d  %d\n",bswitch.v1,bswitch.v2);
    
    return 0;//
}


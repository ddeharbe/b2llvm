#include "opbool_i.llvm.h"
#include <stdio.h>
const bool debug = false;
opbool$state$ state;

void check(bool value , bool expected, char * msg){
    if(value ==expected)

        printf("OK - %d ", value);
    else
        printf("Error - %d ", value);
    printf("%s \n", msg);

}

int main(){
  // Call init
    opbool$init$(&state);

    check(state.var1, 0 , "var1");

    opbool$set_t(&state);
    check(state.var1, 1 , "set_t");

    opbool$set_f(&state);
    check(state.var1, 0 , "set_f");

    opbool$set_t(&state);
    opbool$op0(&state);
    check(state.var1, 0 , "op0");

    opbool$set_t(&state);
    opbool$op1(&state);
    check(state.var1, 0 , "op1");

    opbool$set_f(&state);
    opbool$op2(&state);
    check(state.var1, 1 , "op2");

    opbool$set_f(&state);
    opbool$op3(&state);
    check(state.var1, 1 , "op3");

    opbool$set_t(&state);
    opbool$op4(&state);
    check(state.var1, 0 , "op4");

    opbool$set_t(&state);
    opbool$op5(&state);
    check(state.var1, 0 , "op5");

    opbool$set_t(&state);
    opbool$op6(&state);
    check(state.var1, 0 , "op6");


    opbool$set_f(&state);
    opbool$op7(&state);
    check(state.var1, 1 , "op7");


}

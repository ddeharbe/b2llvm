; ModuleID = 'initc-bubble_i.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.11.0"

%"struct.bubble$state$" = type { [100 x i32], i32 }

@.str = private unnamed_addr constant [10 x i8] c"Original\0A\00", align 1
@bubble = common global %"struct.bubble$state$" zeroinitializer, align 4
@.str1 = private unnamed_addr constant [15 x i8] c" vec1[%d]= %d\0A\00", align 1
@.str2 = private unnamed_addr constant [12 x i8] c"After init\0A\00", align 1
@.str3 = private unnamed_addr constant [6 x i8] c" ...\0A\00", align 1
@.str4 = private unnamed_addr constant [12 x i8] c"After sort\0A\00", align 1
@.str5 = private unnamed_addr constant [16 x i8] c"Error i=%d - %d\00", align 1
@.str6 = private unnamed_addr constant [18 x i8] c"Error - i=%d - %d\00", align 1
@.str7 = private unnamed_addr constant [19 x i8] c"OK - vec1[%d]= %d\0A\00", align 1
@.str8 = private unnamed_addr constant [16 x i8] c"Error in tests!\00", align 1
@.str9 = private unnamed_addr constant [14 x i8] c"All tests ok!\00", align 1

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  %error = alloca i8, align 1
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  %i3 = alloca i32, align 4
  store i32 0, i32* %1
  store i8 0, i8* %error, align 1
  %2 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %i1, align 4
  br label %3

; <label>:3                                       ; preds = %18, %0
  %4 = load i32* %i1, align 4
  %5 = icmp slt i32 %4, 100
  br i1 %5, label %6, label %21

; <label>:6                                       ; preds = %3
  %7 = load i32* %i1, align 4
  %8 = sub nsw i32 100, %7
  %9 = load i32* %i1, align 4
  %10 = sext i32 %9 to i64
  %11 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %10
  store i32 %8, i32* %11, align 4
  %12 = load i32* %i1, align 4
  %13 = load i32* %i1, align 4
  %14 = sext i32 %13 to i64
  %15 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %14
  %16 = load i32* %15, align 4
  %17 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str1, i32 0, i32 0), i32 %12, i32 %16)
  br label %18

; <label>:18                                      ; preds = %6
  %19 = load i32* %i1, align 4
  %20 = add nsw i32 %19, 1
  store i32 %20, i32* %i1, align 4
  br label %3

; <label>:21                                      ; preds = %3
  call void @"bubble$init$"(%"struct.bubble$state$"* @bubble)
  %22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str2, i32 0, i32 0))
  store i32 0, i32* %i2, align 4
  br label %23

; <label>:23                                      ; preds = %33, %21
  %24 = load i32* %i2, align 4
  %25 = icmp slt i32 %24, 100
  br i1 %25, label %26, label %36

; <label>:26                                      ; preds = %23
  %27 = load i32* %i2, align 4
  %28 = load i32* %i2, align 4
  %29 = sext i32 %28 to i64
  %30 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %29
  %31 = load i32* %30, align 4
  %32 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str1, i32 0, i32 0), i32 %27, i32 %31)
  br label %33

; <label>:33                                      ; preds = %26
  %34 = load i32* %i2, align 4
  %35 = add nsw i32 %34, 1
  store i32 %35, i32* %i2, align 4
  br label %23

; <label>:36                                      ; preds = %23
  %37 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([6 x i8]* @.str3, i32 0, i32 0))
  call void @"bubble$op_sort"(%"struct.bubble$state$"* @bubble)
  %38 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str4, i32 0, i32 0))
  store i32 0, i32* %i3, align 4
  br label %39

; <label>:39                                      ; preds = %111, %36
  %40 = load i32* %i3, align 4
  %41 = icmp slt i32 %40, 100
  br i1 %41, label %42, label %114

; <label>:42                                      ; preds = %39
  %43 = load i32* %i3, align 4
  %44 = icmp eq i32 %43, 0
  %45 = zext i1 %44 to i32
  %46 = load i32* %i3, align 4
  %47 = sext i32 %46 to i64
  %48 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %47
  %49 = load i32* %48, align 4
  %50 = icmp ne i32 %49, 0
  %51 = zext i1 %50 to i32
  %52 = and i32 %45, %51
  %53 = icmp ne i32 %52, 0
  br i1 %53, label %54, label %61

; <label>:54                                      ; preds = %42
  %55 = load i32* %i3, align 4
  %56 = load i32* %i3, align 4
  %57 = sext i32 %56 to i64
  %58 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %57
  %59 = load i32* %58, align 4
  %60 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([16 x i8]* @.str5, i32 0, i32 0), i32 %55, i32 %59)
  store i8 1, i8* %error, align 1
  br label %110

; <label>:61                                      ; preds = %42
  %62 = load i32* %i3, align 4
  %63 = icmp eq i32 %62, 1
  %64 = zext i1 %63 to i32
  %65 = load i32* %i3, align 4
  %66 = sext i32 %65 to i64
  %67 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %66
  %68 = load i32* %67, align 4
  %69 = icmp ne i32 %68, 0
  %70 = zext i1 %69 to i32
  %71 = and i32 %64, %70
  %72 = icmp ne i32 %71, 0
  br i1 %72, label %73, label %80

; <label>:73                                      ; preds = %61
  %74 = load i32* %i3, align 4
  %75 = load i32* %i3, align 4
  %76 = sext i32 %75 to i64
  %77 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %76
  %78 = load i32* %77, align 4
  %79 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([16 x i8]* @.str5, i32 0, i32 0), i32 %74, i32 %78)
  store i8 1, i8* %error, align 1
  br label %109

; <label>:80                                      ; preds = %61
  %81 = load i32* %i3, align 4
  %82 = icmp sgt i32 %81, 1
  %83 = zext i1 %82 to i32
  %84 = load i32* %i3, align 4
  %85 = sext i32 %84 to i64
  %86 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %85
  %87 = load i32* %86, align 4
  %88 = load i32* %i3, align 4
  %89 = sub nsw i32 %88, 1
  %90 = icmp ne i32 %87, %89
  %91 = zext i1 %90 to i32
  %92 = and i32 %83, %91
  %93 = icmp ne i32 %92, 0
  br i1 %93, label %94, label %101

; <label>:94                                      ; preds = %80
  %95 = load i32* %i3, align 4
  %96 = load i32* %i3, align 4
  %97 = sext i32 %96 to i64
  %98 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %97
  %99 = load i32* %98, align 4
  %100 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([18 x i8]* @.str6, i32 0, i32 0), i32 %95, i32 %99)
  store i8 1, i8* %error, align 1
  br label %108

; <label>:101                                     ; preds = %80
  %102 = load i32* %i3, align 4
  %103 = load i32* %i3, align 4
  %104 = sext i32 %103 to i64
  %105 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %104
  %106 = load i32* %105, align 4
  %107 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([19 x i8]* @.str7, i32 0, i32 0), i32 %102, i32 %106)
  br label %108

; <label>:108                                     ; preds = %101, %94
  br label %109

; <label>:109                                     ; preds = %108, %73
  br label %110

; <label>:110                                     ; preds = %109, %54
  br label %111

; <label>:111                                     ; preds = %110
  %112 = load i32* %i3, align 4
  %113 = add nsw i32 %112, 1
  store i32 %113, i32* %i3, align 4
  br label %39

; <label>:114                                     ; preds = %39
  %115 = load i8* %error, align 1
  %116 = trunc i8 %115 to i1
  br i1 %116, label %117, label %119

; <label>:117                                     ; preds = %114
  %118 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([16 x i8]* @.str8, i32 0, i32 0))
  br label %121

; <label>:119                                     ; preds = %114
  %120 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([14 x i8]* @.str9, i32 0, i32 0))
  br label %121

; <label>:121                                     ; preds = %119, %117
  %122 = load i32* %1
  ret i32 %122
}

declare i32 @printf(i8*, ...) #1

declare void @"bubble$init$"(%"struct.bubble$state$"*) #1

declare void @"bubble$op_sort"(%"struct.bubble$state$"*) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }


; ModuleID = 'initc-arrayCartesianProduct_i.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.11.0"

%"struct.arrayCartesianProduct$state$" = type { [100 x i32], [100 x [100 x i32]], [100 x i32], [100 x i32], [2 x [2 x [4 x i32]]], [2 x [2 x [4 x i32]]], i32, i32, [100 x i32], [4 x [4 x i32]], [11 x [21 x [31 x i32]]], [2 x [6 x [4 x i32]]] }

@debug = constant i8 0, align 1
@arrayCartesianProduct = common global %"struct.arrayCartesianProduct$state$" zeroinitializer, align 4
@.str = private unnamed_addr constant [10 x i8] c"OK - arr\0A\00", align 1
@.str1 = private unnamed_addr constant [18 x i8] c"%d\0AError line:%d\0A\00", align 1
@.str2 = private unnamed_addr constant [11 x i8] c"OK - tint\0A\00", align 1
@.str3 = private unnamed_addr constant [15 x i8] c"Error line:%d\0A\00", align 1
@.str4 = private unnamed_addr constant [12 x i8] c"OK - arr2d\0A\00", align 1
@.str5 = private unnamed_addr constant [37 x i8] c"arr2d[0][0] = 9 != %d\0AError line:%d\0A\00", align 1
@.str6 = private unnamed_addr constant [37 x i8] c"arr2d[1][1] =99 != %d\0AError line:%d\0A\00", align 1
@.str7 = private unnamed_addr constant [28 x i8] c"Error line:%d  arr:%d->%d \0A\00", align 1
@.str8 = private unnamed_addr constant [22 x i8] c"OK - copy tmp := arr\0A\00", align 1
@.str9 = private unnamed_addr constant [10 x i8] c"OK - a3d\0A\00", align 1
@.str10 = private unnamed_addr constant [37 x i8] c"OK - a3d copy arrayCartesianProduct\0A\00", align 1
@.str11 = private unnamed_addr constant [73 x i8] c"OK - init arrayCartesianProduct with Cartesian Product ( 2 Dimensions) \0A\00", align 1
@.str12 = private unnamed_addr constant [16 x i8] c"%d,%d,%d -> %d \00", align 1
@.str13 = private unnamed_addr constant [84 x i8] c"OK - init arrayCartesianProduct with Cartesian Product ( 3 Dimensions) of integer \0A\00", align 1
@.str14 = private unnamed_addr constant [23 x i8] c"OK - copy tmp2 := arr\0A\00", align 1

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %i1 = alloca i32, align 4
  %j2 = alloca i32, align 4
  %i3 = alloca i32, align 4
  %i4 = alloca i32, align 4
  %j5 = alloca i32, align 4
  %k = alloca i32, align 4
  %error_copy_arrayCartesianProduct = alloca i8, align 1
  %i6 = alloca i32, align 4
  %i7 = alloca i32, align 4
  %error_init_arrayCartesianProduct_CartProduct = alloca i8, align 1
  %i8 = alloca i32, align 4
  %j9 = alloca i32, align 4
  %i10 = alloca i32, align 4
  %j11 = alloca i32, align 4
  %k12 = alloca i32, align 4
  %i13 = alloca i32, align 4
  store i32 0, i32* %1
  store i32 0, i32* %i1, align 4
  br label %2

; <label>:2                                       ; preds = %20, %0
  %3 = load i32* %i1, align 4
  %4 = icmp slt i32 %3, 100
  br i1 %4, label %5, label %23

; <label>:5                                       ; preds = %2
  store i32 0, i32* %j2, align 4
  br label %6

; <label>:6                                       ; preds = %16, %5
  %7 = load i32* %j2, align 4
  %8 = icmp slt i32 %7, 100
  br i1 %8, label %9, label %19

; <label>:9                                       ; preds = %6
  %10 = load i32* %j2, align 4
  %11 = sext i32 %10 to i64
  %12 = load i32* %i1, align 4
  %13 = sext i32 %12 to i64
  %14 = getelementptr inbounds [100 x [100 x i32]]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 1), i32 0, i64 %13
  %15 = getelementptr inbounds [100 x i32]* %14, i32 0, i64 %11
  store i32 2, i32* %15, align 4
  br label %16

; <label>:16                                      ; preds = %9
  %17 = load i32* %j2, align 4
  %18 = add nsw i32 %17, 1
  store i32 %18, i32* %j2, align 4
  br label %6

; <label>:19                                      ; preds = %6
  br label %20

; <label>:20                                      ; preds = %19
  %21 = load i32* %i1, align 4
  %22 = add nsw i32 %21, 1
  store i32 %22, i32* %i1, align 4
  br label %2

; <label>:23                                      ; preds = %2
  store i32 0, i32* %i3, align 4
  br label %24

; <label>:24                                      ; preds = %34, %23
  %25 = load i32* %i3, align 4
  %26 = icmp slt i32 %25, 100
  br i1 %26, label %27, label %37

; <label>:27                                      ; preds = %24
  %28 = load i32* %i3, align 4
  %29 = sext i32 %28 to i64
  %30 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0), i32 0, i64 %29
  store i32 15, i32* %30, align 4
  %31 = load i32* %i3, align 4
  %32 = sext i32 %31 to i64
  %33 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 2), i32 0, i64 %32
  store i32 2, i32* %33, align 4
  br label %34

; <label>:34                                      ; preds = %27
  %35 = load i32* %i3, align 4
  %36 = add nsw i32 %35, 1
  store i32 %36, i32* %i3, align 4
  br label %24

; <label>:37                                      ; preds = %24
  store i32 0, i32* %i4, align 4
  br label %38

; <label>:38                                      ; preds = %67, %37
  %39 = load i32* %i4, align 4
  %40 = icmp sle i32 %39, 1
  br i1 %40, label %41, label %70

; <label>:41                                      ; preds = %38
  store i32 0, i32* %j5, align 4
  br label %42

; <label>:42                                      ; preds = %63, %41
  %43 = load i32* %j5, align 4
  %44 = icmp sle i32 %43, 5
  br i1 %44, label %45, label %66

; <label>:45                                      ; preds = %42
  store i32 0, i32* %k, align 4
  br label %46

; <label>:46                                      ; preds = %59, %45
  %47 = load i32* %k, align 4
  %48 = icmp sle i32 %47, 3
  br i1 %48, label %49, label %62

; <label>:49                                      ; preds = %46
  %50 = load i32* %k, align 4
  %51 = sext i32 %50 to i64
  %52 = load i32* %j5, align 4
  %53 = sext i32 %52 to i64
  %54 = load i32* %i4, align 4
  %55 = sext i32 %54 to i64
  %56 = getelementptr inbounds [2 x [6 x [4 x i32]]]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 11), i32 0, i64 %55
  %57 = getelementptr inbounds [6 x [4 x i32]]* %56, i32 0, i64 %53
  %58 = getelementptr inbounds [4 x i32]* %57, i32 0, i64 %51
  store i32 0, i32* %58, align 4
  br label %59

; <label>:59                                      ; preds = %49
  %60 = load i32* %k, align 4
  %61 = add nsw i32 %60, 1
  store i32 %61, i32* %k, align 4
  br label %46

; <label>:62                                      ; preds = %46
  br label %63

; <label>:63                                      ; preds = %62
  %64 = load i32* %j5, align 4
  %65 = add nsw i32 %64, 1
  store i32 %65, i32* %j5, align 4
  br label %42

; <label>:66                                      ; preds = %42
  br label %67

; <label>:67                                      ; preds = %66
  %68 = load i32* %i4, align 4
  %69 = add nsw i32 %68, 1
  store i32 %69, i32* %i4, align 4
  br label %38

; <label>:70                                      ; preds = %38
  call void @"arrayCartesianProduct$init$"(%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct)
  %71 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0, i64 3), align 4
  %72 = icmp eq i32 %71, 10
  br i1 %72, label %73, label %75

; <label>:73                                      ; preds = %70
  %74 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  br label %78

; <label>:75                                      ; preds = %70
  %76 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0, i64 3), align 4
  %77 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([18 x i8]* @.str1, i32 0, i32 0), i32 %76, i32 48)
  br label %78

; <label>:78                                      ; preds = %75, %73
  %79 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 6), align 4
  %80 = icmp eq i32 %79, 7
  br i1 %80, label %81, label %86

; <label>:81                                      ; preds = %78
  %82 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 7), align 4
  %83 = icmp eq i32 %82, 777
  br i1 %83, label %84, label %86

; <label>:84                                      ; preds = %81
  %85 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([11 x i8]* @.str2, i32 0, i32 0))
  br label %88

; <label>:86                                      ; preds = %81, %78
  %87 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 53)
  br label %88

; <label>:88                                      ; preds = %86, %84
  %89 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 1, i64 0, i64 0), align 4
  %90 = icmp eq i32 %89, 9
  br i1 %90, label %91, label %93

; <label>:91                                      ; preds = %88
  %92 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str4, i32 0, i32 0))
  br label %96

; <label>:93                                      ; preds = %88
  %94 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 1, i64 0, i64 0), align 4
  %95 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([37 x i8]* @.str5, i32 0, i32 0), i32 %94, i32 58)
  br label %96

; <label>:96                                      ; preds = %93, %91
  %97 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 1, i64 1, i64 1), align 4
  %98 = icmp eq i32 %97, 99
  br i1 %98, label %99, label %101

; <label>:99                                      ; preds = %96
  %100 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str4, i32 0, i32 0))
  br label %104

; <label>:101                                     ; preds = %96
  %102 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 1, i64 1, i64 1), align 4
  %103 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([37 x i8]* @.str6, i32 0, i32 0), i32 %102, i32 63)
  br label %104

; <label>:104                                     ; preds = %101, %99
  %105 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 2, i64 1), align 4
  %106 = icmp eq i32 %105, 10
  br i1 %106, label %107, label %109

; <label>:107                                     ; preds = %104
  %108 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  br label %111

; <label>:109                                     ; preds = %104
  %110 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 69)
  br label %111

; <label>:111                                     ; preds = %109, %107
  %112 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0, i64 1), align 4
  %113 = icmp eq i32 %112, 33
  br i1 %113, label %114, label %116

; <label>:114                                     ; preds = %111
  %115 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  br label %118

; <label>:116                                     ; preds = %111
  %117 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 74)
  br label %118

; <label>:118                                     ; preds = %116, %114
  store i8 0, i8* %error_copy_arrayCartesianProduct, align 1
  store i32 0, i32* %i6, align 4
  br label %119

; <label>:119                                     ; preds = %154, %118
  %120 = load i32* %i6, align 4
  %121 = icmp slt i32 %120, 100
  br i1 %121, label %122, label %157

; <label>:122                                     ; preds = %119
  %123 = load i32* %i6, align 4
  %124 = sext i32 %123 to i64
  %125 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0), i32 0, i64 %124
  %126 = load i32* %125, align 4
  %127 = icmp ne i32 %126, 15
  br i1 %127, label %128, label %153

; <label>:128                                     ; preds = %122
  %129 = load i32* %i6, align 4
  %130 = icmp eq i32 %129, 1
  br i1 %130, label %131, label %137

; <label>:131                                     ; preds = %128
  %132 = load i32* %i6, align 4
  %133 = sext i32 %132 to i64
  %134 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0), i32 0, i64 %133
  %135 = load i32* %134, align 4
  %136 = icmp eq i32 %135, 33
  br i1 %136, label %153, label %137

; <label>:137                                     ; preds = %131, %128
  %138 = load i32* %i6, align 4
  %139 = icmp eq i32 %138, 3
  br i1 %139, label %140, label %146

; <label>:140                                     ; preds = %137
  %141 = load i32* %i6, align 4
  %142 = sext i32 %141 to i64
  %143 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0), i32 0, i64 %142
  %144 = load i32* %143, align 4
  %145 = icmp eq i32 %144, 10
  br i1 %145, label %153, label %146

; <label>:146                                     ; preds = %140, %137
  %147 = load i32* %i6, align 4
  %148 = load i32* %i6, align 4
  %149 = sext i32 %148 to i64
  %150 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 0), i32 0, i64 %149
  %151 = load i32* %150, align 4
  %152 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([28 x i8]* @.str7, i32 0, i32 0), i32 80, i32 %147, i32 %151)
  store i8 1, i8* %error_copy_arrayCartesianProduct, align 1
  br label %153

; <label>:153                                     ; preds = %146, %140, %131, %122
  br label %154

; <label>:154                                     ; preds = %153
  %155 = load i32* %i6, align 4
  %156 = add nsw i32 %155, 1
  store i32 %156, i32* %i6, align 4
  br label %119

; <label>:157                                     ; preds = %119
  store i32 0, i32* %i7, align 4
  br label %158

; <label>:158                                     ; preds = %187, %157
  %159 = load i32* %i7, align 4
  %160 = icmp slt i32 %159, 100
  br i1 %160, label %161, label %190

; <label>:161                                     ; preds = %158
  %162 = load i32* %i7, align 4
  %163 = sext i32 %162 to i64
  %164 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 2), i32 0, i64 %163
  %165 = load i32* %164, align 4
  %166 = icmp ne i32 %165, 15
  br i1 %166, label %167, label %186

; <label>:167                                     ; preds = %161
  %168 = load i32* %i7, align 4
  %169 = icmp eq i32 %168, 1
  br i1 %169, label %170, label %173

; <label>:170                                     ; preds = %167
  %171 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 2, i64 1), align 4
  %172 = icmp eq i32 %171, 10
  br i1 %172, label %186, label %173

; <label>:173                                     ; preds = %170, %167
  %174 = load i32* %i7, align 4
  %175 = icmp eq i32 %174, 3
  br i1 %175, label %176, label %179

; <label>:176                                     ; preds = %173
  %177 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 2, i64 3), align 4
  %178 = icmp eq i32 %177, 10
  br i1 %178, label %186, label %179

; <label>:179                                     ; preds = %176, %173
  %180 = load i32* %i7, align 4
  %181 = load i32* %i7, align 4
  %182 = sext i32 %181 to i64
  %183 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 2), i32 0, i64 %182
  %184 = load i32* %183, align 4
  %185 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([28 x i8]* @.str7, i32 0, i32 0), i32 87, i32 %180, i32 %184)
  store i8 1, i8* %error_copy_arrayCartesianProduct, align 1
  br label %186

; <label>:186                                     ; preds = %179, %176, %170, %161
  br label %187

; <label>:187                                     ; preds = %186
  %188 = load i32* %i7, align 4
  %189 = add nsw i32 %188, 1
  store i32 %189, i32* %i7, align 4
  br label %158

; <label>:190                                     ; preds = %158
  %191 = load i8* %error_copy_arrayCartesianProduct, align 1
  %192 = trunc i8 %191 to i1
  br i1 %192, label %195, label %193

; <label>:193                                     ; preds = %190
  %194 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([22 x i8]* @.str8, i32 0, i32 0))
  br label %195

; <label>:195                                     ; preds = %193, %190
  %196 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 4, i64 1, i64 1, i64 3), align 4
  %197 = icmp eq i32 %196, 9
  br i1 %197, label %198, label %206

; <label>:198                                     ; preds = %195
  %199 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 4, i64 0, i64 0, i64 1), align 4
  %200 = icmp eq i32 %199, 100
  br i1 %200, label %201, label %206

; <label>:201                                     ; preds = %198
  %202 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 4, i64 0, i64 1, i64 1), align 4
  %203 = icmp eq i32 %202, 100
  br i1 %203, label %204, label %206

; <label>:204                                     ; preds = %201
  %205 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str9, i32 0, i32 0))
  br label %208

; <label>:206                                     ; preds = %201, %198, %195
  %207 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 98)
  br label %208

; <label>:208                                     ; preds = %206, %204
  %209 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 5, i64 1, i64 0, i64 0), align 4
  %210 = icmp eq i32 %209, 77
  br i1 %210, label %211, label %216

; <label>:211                                     ; preds = %208
  %212 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 4, i64 1, i64 0, i64 0), align 4
  %213 = icmp ne i32 %212, 77
  br i1 %213, label %214, label %216

; <label>:214                                     ; preds = %211
  %215 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([37 x i8]* @.str10, i32 0, i32 0))
  br label %218

; <label>:216                                     ; preds = %211, %208
  %217 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 103)
  br label %218

; <label>:218                                     ; preds = %216, %214
  store i8 0, i8* %error_init_arrayCartesianProduct_CartProduct, align 1
  store i32 0, i32* %i8, align 4
  br label %219

; <label>:219                                     ; preds = %242, %218
  %220 = load i32* %i8, align 4
  %221 = icmp sle i32 %220, 3
  br i1 %221, label %222, label %245

; <label>:222                                     ; preds = %219
  store i32 0, i32* %j9, align 4
  br label %223

; <label>:223                                     ; preds = %238, %222
  %224 = load i32* %j9, align 4
  %225 = icmp sle i32 %224, 3
  br i1 %225, label %226, label %241

; <label>:226                                     ; preds = %223
  %227 = load i32* %j9, align 4
  %228 = sext i32 %227 to i64
  %229 = load i32* %i8, align 4
  %230 = sext i32 %229 to i64
  %231 = getelementptr inbounds [4 x [4 x i32]]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 9), i32 0, i64 %230
  %232 = getelementptr inbounds [4 x i32]* %231, i32 0, i64 %228
  %233 = load i32* %232, align 4
  %234 = icmp ne i32 %233, 4
  br i1 %234, label %235, label %237

; <label>:235                                     ; preds = %226
  store i8 1, i8* %error_init_arrayCartesianProduct_CartProduct, align 1
  %236 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 110)
  br label %237

; <label>:237                                     ; preds = %235, %226
  br label %238

; <label>:238                                     ; preds = %237
  %239 = load i32* %j9, align 4
  %240 = add nsw i32 %239, 1
  store i32 %240, i32* %j9, align 4
  br label %223

; <label>:241                                     ; preds = %223
  br label %242

; <label>:242                                     ; preds = %241
  %243 = load i32* %i8, align 4
  %244 = add nsw i32 %243, 1
  store i32 %244, i32* %i8, align 4
  br label %219

; <label>:245                                     ; preds = %219
  %246 = load i8* %error_init_arrayCartesianProduct_CartProduct, align 1
  %247 = trunc i8 %246 to i1
  br i1 %247, label %250, label %248

; <label>:248                                     ; preds = %245
  %249 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([73 x i8]* @.str11, i32 0, i32 0))
  br label %250

; <label>:250                                     ; preds = %248, %245
  store i8 0, i8* %error_init_arrayCartesianProduct_CartProduct, align 1
  store i32 0, i32* %i10, align 4
  br label %251

; <label>:251                                     ; preds = %299, %250
  %252 = load i32* %i10, align 4
  %253 = icmp sle i32 %252, 10
  br i1 %253, label %254, label %302

; <label>:254                                     ; preds = %251
  store i32 0, i32* %j11, align 4
  br label %255

; <label>:255                                     ; preds = %295, %254
  %256 = load i32* %j11, align 4
  %257 = icmp sle i32 %256, 20
  br i1 %257, label %258, label %298

; <label>:258                                     ; preds = %255
  store i32 0, i32* %k12, align 4
  br label %259

; <label>:259                                     ; preds = %291, %258
  %260 = load i32* %k12, align 4
  %261 = icmp sle i32 %260, 30
  br i1 %261, label %262, label %294

; <label>:262                                     ; preds = %259
  %263 = load i32* %k12, align 4
  %264 = sext i32 %263 to i64
  %265 = load i32* %j11, align 4
  %266 = sext i32 %265 to i64
  %267 = load i32* %i10, align 4
  %268 = sext i32 %267 to i64
  %269 = getelementptr inbounds [11 x [21 x [31 x i32]]]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 10), i32 0, i64 %268
  %270 = getelementptr inbounds [21 x [31 x i32]]* %269, i32 0, i64 %266
  %271 = getelementptr inbounds [31 x i32]* %270, i32 0, i64 %264
  %272 = load i32* %271, align 4
  %273 = icmp ne i32 %272, 10
  br i1 %273, label %274, label %290

; <label>:274                                     ; preds = %262
  store i8 1, i8* %error_init_arrayCartesianProduct_CartProduct, align 1
  %275 = load i32* %i10, align 4
  %276 = load i32* %j11, align 4
  %277 = load i32* %k12, align 4
  %278 = load i32* %k12, align 4
  %279 = sext i32 %278 to i64
  %280 = load i32* %j11, align 4
  %281 = sext i32 %280 to i64
  %282 = load i32* %i10, align 4
  %283 = sext i32 %282 to i64
  %284 = getelementptr inbounds [11 x [21 x [31 x i32]]]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 10), i32 0, i64 %283
  %285 = getelementptr inbounds [21 x [31 x i32]]* %284, i32 0, i64 %281
  %286 = getelementptr inbounds [31 x i32]* %285, i32 0, i64 %279
  %287 = load i32* %286, align 4
  %288 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([16 x i8]* @.str12, i32 0, i32 0), i32 %275, i32 %276, i32 %277, i32 %287)
  %289 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 124)
  br label %290

; <label>:290                                     ; preds = %274, %262
  br label %291

; <label>:291                                     ; preds = %290
  %292 = load i32* %k12, align 4
  %293 = add nsw i32 %292, 1
  store i32 %293, i32* %k12, align 4
  br label %259

; <label>:294                                     ; preds = %259
  br label %295

; <label>:295                                     ; preds = %294
  %296 = load i32* %j11, align 4
  %297 = add nsw i32 %296, 1
  store i32 %297, i32* %j11, align 4
  br label %255

; <label>:298                                     ; preds = %255
  br label %299

; <label>:299                                     ; preds = %298
  %300 = load i32* %i10, align 4
  %301 = add nsw i32 %300, 1
  store i32 %301, i32* %i10, align 4
  br label %251

; <label>:302                                     ; preds = %251
  %303 = load i8* %error_init_arrayCartesianProduct_CartProduct, align 1
  %304 = trunc i8 %303 to i1
  br i1 %304, label %307, label %305

; <label>:305                                     ; preds = %302
  %306 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([84 x i8]* @.str13, i32 0, i32 0))
  br label %307

; <label>:307                                     ; preds = %305, %302
  call void @"arrayCartesianProduct$op0"(%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct)
  store i32 0, i32* %i13, align 4
  br label %308

; <label>:308                                     ; preds = %337, %307
  %309 = load i32* %i13, align 4
  %310 = icmp slt i32 %309, 100
  br i1 %310, label %311, label %340

; <label>:311                                     ; preds = %308
  %312 = load i32* %i13, align 4
  %313 = sext i32 %312 to i64
  %314 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 2), i32 0, i64 %313
  %315 = load i32* %314, align 4
  %316 = icmp ne i32 %315, 15
  br i1 %316, label %317, label %336

; <label>:317                                     ; preds = %311
  %318 = load i32* %i13, align 4
  %319 = icmp eq i32 %318, 1
  br i1 %319, label %320, label %323

; <label>:320                                     ; preds = %317
  %321 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 3, i64 1), align 4
  %322 = icmp eq i32 %321, 33
  br i1 %322, label %336, label %323

; <label>:323                                     ; preds = %320, %317
  %324 = load i32* %i13, align 4
  %325 = icmp eq i32 %324, 3
  br i1 %325, label %326, label %329

; <label>:326                                     ; preds = %323
  %327 = load i32* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 3, i64 3), align 4
  %328 = icmp eq i32 %327, 10
  br i1 %328, label %336, label %329

; <label>:329                                     ; preds = %326, %323
  %330 = load i32* %i13, align 4
  %331 = load i32* %i13, align 4
  %332 = sext i32 %331 to i64
  %333 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.arrayCartesianProduct$state$"* @arrayCartesianProduct, i32 0, i32 3), i32 0, i64 %332
  %334 = load i32* %333, align 4
  %335 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([28 x i8]* @.str7, i32 0, i32 0), i32 137, i32 %330, i32 %334)
  store i8 1, i8* %error_copy_arrayCartesianProduct, align 1
  br label %336

; <label>:336                                     ; preds = %329, %326, %320, %311
  br label %337

; <label>:337                                     ; preds = %336
  %338 = load i32* %i13, align 4
  %339 = add nsw i32 %338, 1
  store i32 %339, i32* %i13, align 4
  br label %308

; <label>:340                                     ; preds = %308
  %341 = load i8* %error_copy_arrayCartesianProduct, align 1
  %342 = trunc i8 %341 to i1
  br i1 %342, label %345, label %343

; <label>:343                                     ; preds = %340
  %344 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([23 x i8]* @.str14, i32 0, i32 0))
  br label %345

; <label>:345                                     ; preds = %343, %340
  %346 = load i32* %1
  ret i32 %346
}

declare void @"arrayCartesianProduct$init$"(%"struct.arrayCartesianProduct$state$"*) #1

declare i32 @printf(i8*, ...) #1

declare void @"arrayCartesianProduct$op0"(%"struct.arrayCartesianProduct$state$"*) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }


#ifndef __Users_valerio_Dropbox_Doutorado_workspace_b2llvm_tests_and_examples_general_examples_c_tests_arrayCartesianProduct_illvm_h
#define __Users_valerio_Dropbox_Doutorado_workspace_b2llvm_tests_and_examples_general_examples_c_tests_arrayCartesianProduct_illvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t arr[100]; int32_t arr2d[100][100]; int32_t tmp[100]; int32_t tmp2[100]; int32_t a3d[2][2][4]; int32_t a3dcopy[2][2][4]; int32_t tint1; int32_t tint2; int32_t arrinit[100]; int32_t arr2CProduct[4][4]; int32_t arr3Cproduct[11][21][31]; int32_t arr4CprodBool[2][6][4]; }   arrayCartesianProduct$state$;
typedef arrayCartesianProduct$state$ * arrayCartesianProduct$ref$;
extern void arrayCartesianProduct$init$(arrayCartesianProduct$ref$ self);
extern void arrayCartesianProduct$op0(arrayCartesianProduct$ref$ self);
extern void arrayCartesianProduct$op1(arrayCartesianProduct$ref$ self);
#endif /* __Users_valerio_Dropbox_Doutorado_workspace_b2llvm_tests_and_examples_general_examples_c_tests_arrayCartesianProduct_illvm_h */

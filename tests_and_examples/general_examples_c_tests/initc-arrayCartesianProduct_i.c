#include "arrayCartesianProduct_i.llvm.h"
#include <stdio.h>
const bool debug = false;
arrayCartesianProduct$state$ arrayCartesianProduct;

int main(){
	int i,j;
    // Manual reset
    for(int i=0;i<100;i++){
        for(int j=0;j<100;j++){
        	arrayCartesianProduct.arr2d[i][j]=2;
        }
    }
   // Manual reset
    for(int i=0;i<100;i++){
        arrayCartesianProduct.arr[i]=15;
        arrayCartesianProduct.tmp[i]=2;
    }
    // Manual Reset
    for(int i=0;i<=1;i++){
        for(int j=0;j<=5;j++){
            for(int k=0;k<=3;k++){
                arrayCartesianProduct.arr4CprodBool[i][j][k]=false;
            }
        }
    }

    
    
    
    // Call init
    arrayCartesianProduct$init$(&arrayCartesianProduct);
    
    if (debug){
    for(int i=0;i<11;i++){
        for(int j=0;j<11;j++){
            printf(" [%d][%d]=%d\n",i, j, arrayCartesianProduct.arr2d[i][j]);
        }
        printf("\n");
    }
    }
    
    

    if(arrayCartesianProduct.arr[3]==10)
        printf("OK - arr\n");
    else
        printf("%d\nError line:%d\n",arrayCartesianProduct.arr[3],__LINE__);
    
    if(arrayCartesianProduct.tint1==7 && arrayCartesianProduct.tint2==777 )
        printf("OK - tint\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    if(arrayCartesianProduct.arr2d[0][0]==9)
        printf("OK - arr2d\n");
    else
        printf("arr2d[0][0] = 9 != %d\nError line:%d\n",arrayCartesianProduct.arr2d[0][0],__LINE__);
    
    if(arrayCartesianProduct.arr2d[1][1]==99)
        printf("OK - arr2d\n");
    else
        printf("arr2d[1][1] =99 != %d\nError line:%d\n",arrayCartesianProduct.arr2d[1][1],__LINE__);
    
    
    if(arrayCartesianProduct.tmp[1]==10)
        printf("OK - arr\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    if(arrayCartesianProduct.arr[1]==33)
        printf("OK - arr\n");
    else
        printf("Error line:%d\n",__LINE__);

    bool error_copy_arrayCartesianProduct=false;
    
    for(int i=0;i<100;i++){
        if( arrayCartesianProduct.arr[i]!= 15 && !( i==1 && arrayCartesianProduct.arr[i]==33) && !( i==3 && arrayCartesianProduct.arr[i]==10) ){
            printf("Error line:%d  arr:%d->%d \n",__LINE__,i,arrayCartesianProduct.arr[i]);
            error_copy_arrayCartesianProduct=true;
        }
    }

    for(int i=0;i<100;i++){
        if( arrayCartesianProduct.tmp[i]!= 15 && !( i==1 && arrayCartesianProduct.tmp[1]==10) && !( i==3 && arrayCartesianProduct.tmp[3]==10)  ){
            printf("Error line:%d  arr:%d->%d \n",__LINE__,i,arrayCartesianProduct.tmp[i]);
                        error_copy_arrayCartesianProduct=true;
        }
    }
    if(!error_copy_arrayCartesianProduct)
        printf("OK - copy tmp := arr\n");
    
    
    if(arrayCartesianProduct.a3d[1][1][3]==9 && arrayCartesianProduct.a3d[0][0][1]==100 && arrayCartesianProduct.a3d[0][1][1]==100)
        printf("OK - a3d\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    if(arrayCartesianProduct.a3dcopy[1][0][0]==77 && arrayCartesianProduct.a3d[1][0][0]!=77)
        printf("OK - a3d copy arrayCartesianProduct\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    bool error_init_arrayCartesianProduct_CartProduct =false;
    for(int i=0;i<=3;i++){
        for(int j=0;j<=3;j++){
            if (arrayCartesianProduct.arr2CProduct[i][j]!=4){
                error_init_arrayCartesianProduct_CartProduct=true;
                printf("Error line:%d\n",__LINE__);
            }
        }
    }
    if(!error_init_arrayCartesianProduct_CartProduct)
    printf("OK - init arrayCartesianProduct with Cartesian Product ( 2 Dimensions) \n");
    
    error_init_arrayCartesianProduct_CartProduct =false;
    for(int i=0;i<=10;i++){
        for(int j=0;j<=20;j++){
            for(int k=0;k<=30;k++){
                if (arrayCartesianProduct.arr3Cproduct[i][j][k]!=10){
                    error_init_arrayCartesianProduct_CartProduct=true;
                    printf("%d,%d,%d -> %d ", i,j,k,arrayCartesianProduct.arr3Cproduct[i][j][k]);
                    printf("Error line:%d\n",__LINE__);
                }
            }
        }
    }
    if(!error_init_arrayCartesianProduct_CartProduct)
        printf("OK - init arrayCartesianProduct with Cartesian Product ( 3 Dimensions) of integer \n");
    
    
    
    arrayCartesianProduct$op0(&arrayCartesianProduct);
    for(int i=0;i<100;i++){
        if( arrayCartesianProduct.tmp[i]!= 15 && !( i==1 && arrayCartesianProduct.tmp2[1]==33) && !( i==3 && arrayCartesianProduct.tmp2[3]==10)  ){
            printf("Error line:%d  arr:%d->%d \n",__LINE__,i,arrayCartesianProduct.tmp2[i]);
            error_copy_arrayCartesianProduct=true;
        }
    }
    if(!error_copy_arrayCartesianProduct)
    printf("OK - copy tmp2 := arr\n");
    
    
    
  
/* Case to verify - may be the error is related to array of boolean.
    
    error_init_arrayCartesianProduct_CartProduct =false;
    for(int i=0;i<=1;i++){
        for(int j=0;j<=5;j++){
            for(int k=0;k<=3;k++){
                if (arrayCartesianProduct.arr4CprodBool[i][j][k]!=1){
                    error_init_arrayCartesianProduct_CartProduct=true;
                    printf("%d,%d,%d -> %i ", i,j,k,arrayCartesianProduct.arr4CprodBool[i][j][k]);
                    printf("Error line:%d\n",__LINE__);
                }
            }
        }
    }
    if(!error_init_arrayCartesianProduct_CartProduct)
        printf("OK - init arrayCartesianProduct with Cartesian Product ( 3 Dimensions) of bool \n");
    else
        printf("ERRO - init arrayCartesianProduct with Cartesian Product ( 3 Dimensions) of bool \n");
    
    
    
     arrayCartesianProduct$op1(&arrayCartesianProduct);
    error_init_arrayCartesianProduct_CartProduct =false;
    for(int i=0;i<=1;i++){
        for(int j=0;j<=5;j++){
            for(int k=0;k<=3;k++){
                if (arrayCartesianProduct.arr4CprodBool[i][j][k]!=1){
                    error_init_arrayCartesianProduct_CartProduct=true;
                    printf("%d,%d,%d -> %i ", i,j,k,arrayCartesianProduct.arr4CprodBool[i][j][k]);
                    printf("Error line:%d\n",__LINE__);
                }
            }
        }
    }
    if(!error_init_arrayCartesianProduct_CartProduct)
        printf("OK -op1- init arrayCartesianProduct with Cartesian Product ( 3 Dimensions) of bool \n");
    else
        printf("ERRO -op1- init arrayCartesianProduct with Cartesian Product ( 3 Dimensions) of bool \n");
     */

    
    
    
    
    

}


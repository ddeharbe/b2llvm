#!/bin/bash
machine=opbool_i
set_test=general_examples
currentPath="$(pwd)"
cd ..
predPath="$(pwd)"
cd $currentPath
#rm executable *.s $machine.llvm $machine.llvm.h
echo "Calling B2LLVM:"
python2.7 ../../b2llvm.py --verbose  -m comp $machine $machine.llvm $predPath/${set_test}_b project.xml   $predPath/${set_test}_c_tests

echo "Compile initc-${machine}.c:"
clang -S -emit-llvm -fprofile-instr-generate -fcoverage-mapping -c initc-$machine.c
echo "LLC files:"
python shorten.py 5  initc-$machine.ll >/dev/null
llc-mp-3.5  $machine.llvm
llc-mp-3.5  initc-$machine.ll
echo "Linking the files:"
clang --coverage -fprofile-instr-generate -fcoverage-mapping   $machine.llvm.s  initc-$machine.s -o executable
./executable
llvm-profdata-mp-3.5 merge -o executable.profdata default.profraw
llvm-cov-mp-3.5 show ./executable -instr-profile=executable.profdata *.c *.llvm *.ll
#rm  $machine.llvm $machine.llvm.h
#rm executable *.s

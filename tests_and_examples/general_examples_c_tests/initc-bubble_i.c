#include "bubble_i.llvm.h"
#include <stdio.h>

bubble$state$ bubble;

int main(){
    bool error =false;
	int i;
    printf("Original\n");
    for(int i=0;i<100;i++){
        bubble.vec1[i]=100-i;
        printf(" vec1[%d]= %d\n",i, bubble.vec1[i]  );
    }

	bubble$init$(&bubble);
    printf("After init\n");
	for(int i=0;i<100;i++)
        printf(" vec1[%d]= %d\n",i, bubble.vec1[i]  );
    printf(" ...\n");

	bubble$op_sort(&bubble);
    printf("After sort\n");
    for(int i=0;i<100;i++)
        if(i==0 & bubble.vec1[i]!= 0 ){
            printf("Error i=%d - %d",i , bubble.vec1[i]);
            error =true;
        }
        else if(i==1 & bubble.vec1[i]!= 0 ){
            printf("Error i=%d - %d",i , bubble.vec1[i]);
            error =true;
        }
        else if(i>1 & bubble.vec1[i] != i-1  ){
            printf("Error - i=%d - %d",i , bubble.vec1[i]);
            error =true;
        }
        else
            printf("OK - vec1[%d]= %d\n",i, bubble.vec1[i]  );
    
    if (error)
        printf("Error in tests!" );
    else
        printf("All tests ok!" );
    

} 


; ModuleID = 'initc-opbool_i.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.11.0"

%"struct.opbool$state$" = type { i8 }

@debug = constant i8 0, align 1
@.str = private unnamed_addr constant [9 x i8] c"OK - %d \00", align 1
@.str1 = private unnamed_addr constant [12 x i8] c"Error - %d \00", align 1
@.str2 = private unnamed_addr constant [5 x i8] c"%s \0A\00", align 1
@state = common global %"struct.opbool$state$" zeroinitializer, align 1
@.str3 = private unnamed_addr constant [5 x i8] c"var1\00", align 1
@.str4 = private unnamed_addr constant [6 x i8] c"set_t\00", align 1
@.str5 = private unnamed_addr constant [6 x i8] c"set_f\00", align 1
@.str6 = private unnamed_addr constant [4 x i8] c"op0\00", align 1
@.str7 = private unnamed_addr constant [4 x i8] c"op1\00", align 1
@.str8 = private unnamed_addr constant [4 x i8] c"op2\00", align 1
@.str9 = private unnamed_addr constant [4 x i8] c"op3\00", align 1
@.str10 = private unnamed_addr constant [4 x i8] c"op4\00", align 1
@.str11 = private unnamed_addr constant [4 x i8] c"op5\00", align 1
@.str12 = private unnamed_addr constant [4 x i8] c"op6\00", align 1
@.str13 = private unnamed_addr constant [4 x i8] c"op7\00", align 1

; Function Attrs: nounwind ssp uwtable
define void @check(i1 zeroext %value, i1 zeroext %expected, i8* %msg) #0 {
  %1 = alloca i8, align 1
  %2 = alloca i8, align 1
  %3 = alloca i8*, align 8
  %4 = zext i1 %value to i8
  store i8 %4, i8* %1, align 1
  %5 = zext i1 %expected to i8
  store i8 %5, i8* %2, align 1
  store i8* %msg, i8** %3, align 8
  %6 = load i8* %1, align 1
  %7 = trunc i8 %6 to i1
  %8 = zext i1 %7 to i32
  %9 = load i8* %2, align 1
  %10 = trunc i8 %9 to i1
  %11 = zext i1 %10 to i32
  %12 = icmp eq i32 %8, %11
  br i1 %12, label %13, label %18

; <label>:13                                      ; preds = %0
  %14 = load i8* %1, align 1
  %15 = trunc i8 %14 to i1
  %16 = zext i1 %15 to i32
  %17 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([9 x i8]* @.str, i32 0, i32 0), i32 %16)
  br label %23

; <label>:18                                      ; preds = %0
  %19 = load i8* %1, align 1
  %20 = trunc i8 %19 to i1
  %21 = zext i1 %20 to i32
  %22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str1, i32 0, i32 0), i32 %21)
  br label %23

; <label>:23                                      ; preds = %18, %13
  %24 = load i8** %3, align 8
  %25 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([5 x i8]* @.str2, i32 0, i32 0), i8* %24)
  ret void
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  call void @"opbool$init$"(%"struct.opbool$state$"* @state)
  %1 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %2 = trunc i8 %1 to i1
  call void @check(i1 zeroext %2, i1 zeroext false, i8* getelementptr inbounds ([5 x i8]* @.str3, i32 0, i32 0))
  call void @"opbool$set_t"(%"struct.opbool$state$"* @state)
  %3 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %4 = trunc i8 %3 to i1
  call void @check(i1 zeroext %4, i1 zeroext true, i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0))
  call void @"opbool$set_f"(%"struct.opbool$state$"* @state)
  %5 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %6 = trunc i8 %5 to i1
  call void @check(i1 zeroext %6, i1 zeroext false, i8* getelementptr inbounds ([6 x i8]* @.str5, i32 0, i32 0))
  call void @"opbool$set_t"(%"struct.opbool$state$"* @state)
  call void @"opbool$op0"(%"struct.opbool$state$"* @state)
  %7 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %8 = trunc i8 %7 to i1
  call void @check(i1 zeroext %8, i1 zeroext false, i8* getelementptr inbounds ([4 x i8]* @.str6, i32 0, i32 0))
  call void @"opbool$set_t"(%"struct.opbool$state$"* @state)
  call void @"opbool$op1"(%"struct.opbool$state$"* @state)
  %9 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %10 = trunc i8 %9 to i1
  call void @check(i1 zeroext %10, i1 zeroext false, i8* getelementptr inbounds ([4 x i8]* @.str7, i32 0, i32 0))
  call void @"opbool$set_f"(%"struct.opbool$state$"* @state)
  call void @"opbool$op2"(%"struct.opbool$state$"* @state)
  %11 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %12 = trunc i8 %11 to i1
  call void @check(i1 zeroext %12, i1 zeroext true, i8* getelementptr inbounds ([4 x i8]* @.str8, i32 0, i32 0))
  call void @"opbool$set_f"(%"struct.opbool$state$"* @state)
  call void @"opbool$op3"(%"struct.opbool$state$"* @state)
  %13 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %14 = trunc i8 %13 to i1
  call void @check(i1 zeroext %14, i1 zeroext true, i8* getelementptr inbounds ([4 x i8]* @.str9, i32 0, i32 0))
  call void @"opbool$set_t"(%"struct.opbool$state$"* @state)
  call void @"opbool$op4"(%"struct.opbool$state$"* @state)
  %15 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %16 = trunc i8 %15 to i1
  call void @check(i1 zeroext %16, i1 zeroext false, i8* getelementptr inbounds ([4 x i8]* @.str10, i32 0, i32 0))
  call void @"opbool$set_t"(%"struct.opbool$state$"* @state)
  call void @"opbool$op5"(%"struct.opbool$state$"* @state)
  %17 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %18 = trunc i8 %17 to i1
  call void @check(i1 zeroext %18, i1 zeroext false, i8* getelementptr inbounds ([4 x i8]* @.str11, i32 0, i32 0))
  call void @"opbool$set_t"(%"struct.opbool$state$"* @state)
  call void @"opbool$op6"(%"struct.opbool$state$"* @state)
  %19 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %20 = trunc i8 %19 to i1
  call void @check(i1 zeroext %20, i1 zeroext false, i8* getelementptr inbounds ([4 x i8]* @.str12, i32 0, i32 0))
  call void @"opbool$set_f"(%"struct.opbool$state$"* @state)
  call void @"opbool$op7"(%"struct.opbool$state$"* @state)
  %21 = load i8* getelementptr inbounds (%"struct.opbool$state$"* @state, i32 0, i32 0), align 1
  %22 = trunc i8 %21 to i1
  call void @check(i1 zeroext %22, i1 zeroext true, i8* getelementptr inbounds ([4 x i8]* @.str13, i32 0, i32 0))
  ret i32 0
}

declare void @"opbool$init$"(%"struct.opbool$state$"*) #1

declare void @"opbool$set_t"(%"struct.opbool$state$"*) #1

declare void @"opbool$set_f"(%"struct.opbool$state$"*) #1

declare void @"opbool$op0"(%"struct.opbool$state$"*) #1

declare void @"opbool$op1"(%"struct.opbool$state$"*) #1

declare void @"opbool$op2"(%"struct.opbool$state$"*) #1

declare void @"opbool$op3"(%"struct.opbool$state$"*) #1

declare void @"opbool$op4"(%"struct.opbool$state$"*) #1

declare void @"opbool$op5"(%"struct.opbool$state$"*) #1

declare void @"opbool$op6"(%"struct.opbool$state$"*) #1

declare void @"opbool$op7"(%"struct.opbool$state$"*) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }


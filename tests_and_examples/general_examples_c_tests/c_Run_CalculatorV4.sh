#
# This is a crude script to generate coverage data from the set of benchmarks
# that we currently have.
# It relies on the installation of the Python script "coverage"
# https://bitbucket.org/ned/coveragepy
#
#!/bin/bash
COV_RUN="coverage run --source=b2llvm.py,b2llvm -p"
BETA_SHARED="/Users/valerio/Dropbox/BETA_B2LLVM/"
B_project="/Users/valerio/Dropbox/B_Resources/BetaB2LLVM"
bdp="/bdp"
projectName="Calculator"
path_beta_tests="beta_tests"
cd ..
pathB2LLVM=$(pwd)

# Simple example BETA
echo "Copiando \n"
cd $B_project
cp *.mch  $pathB2LLVM"/examples"
cp *.imp  $pathB2LLVM"/examples"
cp bdp/*.bxml  $pathB2LLVM"/bxml"
# Atualizando o BetaTest
echo "BetaB2LLVM"
cp ${projectName:0:3}*.mch ${projectName:0:3}*.imp $pathB2LLVM"/"$path_beta_tests"/"$projectName
mkdir $pathB2LLVM"/"$path_beta_tests"/"$projectName"/bxml"
cp bdp/${projectName:0:3}*.bxml  $pathB2LLVM"/"$path_beta_tests"/"$projectName"/bxml"
pwd 
echo $pathB2LLVM"/"$path_beta_tests$projectName"/bxml"


#TODO - CHAMAR O GERADOR DE BXML (QUANDO DISPONIVEL NO OSX)

echo " executando o B2LLVM em "$projectName
cd $pathB2LLVM

#$COV_RUN ./b2llvm.py --verbose  -m skeleton Calculator_i $projectName".llvm" bxml project.xml
$COV_RUN ./b2llvm.py --verbose  -m comp Calculator_i $projectName".llvm" tests_and_examples/general_examples_b/ project.xml
#$COV_RUN ./b2llvm.py -m proj BubbleSort init-BubbleSort.llvm bxml project.xml
#echo "END BUBBLE NEW"

rm -rf  $pathB2LLVM"/"$path_beta_tests"/"$projectName"/lang"
mkdir $pathB2LLVM"/"$path_beta_tests"/"$projectName"/lang"
cp $pathB2LLVM"/"$projectName".llvm"   $pathB2LLVM"/"$path_beta_tests"/"$projectName"/lang"
cp $pathB2LLVM"/"$projectName".h"   $pathB2LLVM"/"$path_beta_tests"/"$projectName"/lang"

echo "Copiando arquivos"
cp -rvf  $pathB2LLVM"/"$path_beta_tests"/"$projectName  $BETA_SHARED
echo "Compila tudo"
cd $pathB2LLVM"/"$path_beta_tests"/"$projectName"/lang"
llc $projectName".llvm"
clang -S -emit-llvm -c Test.c
llc Test.ll
clang -o execute $projectName".llvm.s" Test.s
./execute





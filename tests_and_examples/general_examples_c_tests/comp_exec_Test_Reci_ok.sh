#!/bin/bash
machine=Reci
set_test=general_examples
currentPath="$(pwd)"
cd ..
predPath="$(pwd)"
cd $currentPath
#rm executable *.s $machine.llvm $machine.llvm.h
echo "Calling B2LLVM:"
python2.7 ../../b2llvm.py --verbose  -m comp $machine $machine.llvm $predPath/${set_test}_b project.xml   $predPath/${set_test}_c_tests

echo "Compile initc-${machine}.c:"
clang -S -emit-llvm -c initc-$machine.c
echo "Cut files:"
#python shorten.py 5  initc-$machine.ll >/dev/null
sed -e :a -e '$d;N;2,7ba' -e 'P;D' initc-$machine.ll>tmp
mv tmp initc-$machine.ll
echo "LLC files:"
llc-mp-3.5  $machine.llvm
llc-mp-3.5  initc-$machine.ll
echo "Linking the files:"
clang -o executable $machine.llvm.s  initc-$machine.s
./executable
#rm  $machine.llvm $machine.llvm.h
rm executable *.s

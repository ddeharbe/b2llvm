B2LLVM = ./b2llvm.py
LLVM_AS = llvm-as-mp-3.5
LLVM_LINK = llvm-link-mp-3.5


%.llvm : bxml/%.bxml
	$(B2LLVM) $* $@ bxml project.xml

init-%.llvm : bxml/%.bxml
	$(B2LLVM) --mode proj $* $@ bxml project.xml
default:
	./selftest.py

build : b2llvm.spec.build.sh
	./b2llvm.spec.build.sh

binary:
	pyinstaller -n b2llvm -F b2llvm.spec
	rm -rf build

%.bc : %.llvm
	$(LLVM_AS) $< -o $@

bxml/counter.bxml: bxml/counter_i.bxml
	touch bxml/counter.bxml

bxml/wd.bxml: bxml/wd_i.bxml
	touch bxml/wd.bxml

bxml/timer.bxml: bxml/timer_i.bxml
	touch bxml/timer.bxml

bxml/enumeration.bxml: bxml/enumeration_i.bxml
	touch bxml/enumeration.bxml

enumeration: enumeration.bc scaffold-enumeration.bc init-enumeration.bc
	$(LLVM_LINK) $^ -o $@

counter: counter.bc scaffold-counter.bc init-counter.bc
	$(LLVM_LINK) $^ -o $@

wd: counter.bc wd.bc init-wd.bc scaffold-wd.bc
	$(LLVM_LINK) $^ -o $@

timer: counter.bc timer.bc init-timer.bc scaffold-timer.bc
	$(LLVM_LINK) $^ -o $@

old_all: counter wd timer

install:
	python2.7 configAtelierB.py

all:
	python3 selftest.py


clean:
	rm .coverage*  *.s *.llvm *.h general_examples_llvm/*.c general_examples_llvm/*.h general_examples_llvm/*.bc general_examples_llvm/*.llvm general_examples_llvm/*.llvm.s tests_and_examples/general_examples_expected_llvm/*.diff  tests_and_examples/general_examples_llvm/storemax_i.llvm 
	python3 selftest.py clean

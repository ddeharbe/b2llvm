#destination="/Users/valerio/Applications/BTools/"
read -p "Please type the destination path (ex:/Users/valerio/Applications/BTools/):" destination
zip=$destination"b2llvm.zip"
find . -type f -name '*.pyc' -exec rm {} +
zip -r b2llvm.zip b2llvm b2llvm.py b2llvm.org configAtelierB.py README -x "*.DS_Store"
cp b2llvm.zip $destination
mv b2llvm.zip dist
unzip -v -f $zip
unzip -o $zip -d $destination
#The following command generate binary b2llvm (translated to C binary)
#pyinstaller b2llvm.spec

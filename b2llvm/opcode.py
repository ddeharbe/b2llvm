'''
This module provides string templates to form lines of LLVM source code.

The templates are properly indented.
'''

_TB = "  "
_NL = "\n"

ALLOC = _TB + "{0} = alloca {1}" + _NL
APPLY = _TB + "{0} = {1} {2} {3}, {4}" + _NL
CALL  = _TB + "call void {0}({1})" + _NL
CGOTO = _TB + "br i1 {0}, label %{1}, label %{2}" + _NL
COMM  = ";; {0}" + _NL
BITCAST  = _TB +"{0} = bitcast {1} {2} to i8*" + _NL
FNDEC = "declare void {0}({1})" + _NL
FNDEF = "define void {0}({1}) {{" + _NL
FNEND = "}}" + _NL
GLOBL = "{0} = common global {1} zeroinitializer" + _NL
GOTO  = _TB + "br label %{0}" + _NL
ICOMP = _TB + "{0} = icmp {1} {2}, {3}" + _NL
LABEL = "{0}:" + _NL
LOADD = _TB + "{0} = load {1}* {2}" + _NL
LOADI = _TB + "{0} = getelementptr {1} {2}, i32 0, i32 {3}" + _NL
GETPTINDEX = _TB + "{0} = getelementptr {1}* {2}, i32 0 {3}" + _NL
OTYPE = "{0} = type opaque" + _NL
RET   = _TB + "ret void" + _NL
STORE = _TB + "store {0} {1}, {0}* {2}" + _NL
SWEND = _TB + "]" + _NL
SWEXP = _TB + "switch {0} {1}, label %{2} [" + _NL
SWVAL = 2*_TB + "{0} {1}, label %{2}" + _NL
TYPE = "{0} = type {1}" + _NL

# C opcodes 

EXTFNDEC = "extern void {0}({1});" + _NL
INCLUDE = "#include {0}" + _NL
IFNDEF = "#ifndef _{0}_h"+ _NL + "#define _{0}_h"+_NL+_NL
ENDIFNDEF = "#endif /* _{0}_h */"+_NL
TYPEDEFR = "typedef {0} {1};" + _NL

# Functions
MEMCPY = "call void @llvm.memcpy.p0i8.p0i8.i64(i8* {0}, i8* {1}, i64 {2}, i32 0, i1 false)"+ _NL
DEC_MEMCPY = "; Function Attrs: nounwind" + _NL+ "declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly,i64,i32,i1)"+ _NL

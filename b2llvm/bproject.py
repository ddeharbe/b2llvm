"""Module responsible for representing B project settings.

B project settings inform what are the developed and base machines. For the
developed machine, the settings additionally record what is the corresponding
implementation.

"""
from b2llvm.strutils import commas
import xml.etree.ElementTree as ET

def text(elem, child):
    """Returns text of XML element child."""
    res  = elem.find(child)
    if res == None: 
        return None
    return elem.find(child).text

class BProject(object):
    '''
    Class representing B projects settings.
    '''
    def __init__(self, name):
        '''
        Constructor, takes as input the path of a bproject.xml file.
        '''
        root = ET.parse(name).getroot()
        implements = root.findall("./developed")
        self.implement = { text(e, "./implementation"):text(e, "./machine")
                           for e in implements }
        foreign = root.findall("./base")
        self.developed = self.implement.values()
        self.base = { e.find("./machine").text for e in foreign }

    def is_developed(self, mach):
        ''' Tests if a machine is a developed machine.

        Inputs:
        - mach: a machine name
        Returns:
        True iff m is declared as developed.
        '''
        return mach in self.developed

    def is_base(self, mach):
        ''' Tests if a machine is a base machine.

        Inputs:
        - m: a machine name
        Returns:
        True iff m is declared as base.
        '''
        return mach in self.base

    def has(self, mach):
        ''' Tests if a machine belongs to a B project

        Inputs:
        - m: a machine name
        Returns:
        True if m is a developed or a base machine declared in project,
        False otherwise.
        '''
        return self.is_developed(mach) or self.is_base(mach)

    def implementation(self, mach):
        """Gets implementation of machine."""
        for imp, machine in self.implement.iteritems():
            if mach == machine:
                assert self.is_developed(mach)
                print("warning: The used implementation of " + mach+" was "+ imp+".")
                return imp

    def implements_str(self):
        "Return list of strings formed by each machine and implementation."
        return [ i + ":" + m for i, m in self.implement.items()]
    
    def get_machine(self, mach):
        """Gets its respective machine."""
        if not(mach in self.implement.keys()):
            print("Warning: the input component is not a B implementation!")
            exit()
        elif mach in self.implement.keys():
            for i, m in self.implement.iteritems():
                if i == mach:
                    return m
        else:
            assert False, "Cannot find " + mach + " in project file."


    def __str__(self):
        return ("developed = {" + commas(self.developed) + "}\n" +
                "implement = {" + commas(self.implements_str()) + "}\n" +
                "base = {" + commas(self.base) + "}\n")

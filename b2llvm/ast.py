"""A Python representation for AST of B machines and implementations.

1. Design

- AST nodes are coded as dict() objects. It would be nicer to have proper
classes, I suppose.

2. Shortcomings

- The full B language is not yet represented. You need to read the code
to know what is supported.
- The AST nodes are somehow incomplete in some cases. Their design is
oriented towards the implementation of the translation to LLVM.
"""
import b2llvm.printer as printer
### TYPES ###

# We are only concerned with representing concrete data.
# The following types of concrete data are supported
# - INTEGERS
# - BOOL
# - Enumerations
# The following types of concrete data are partially supported 
# and are being handled by Valerio, the corresponding code is
# present elsewhere and shall eventually be merged in this section:
# - Arrays (=> Valerio)
# - Intervals (=> Valerio)
#

NATURAL = { "kind": "Nat", "id" : "NATURAL" }
INTEGER = { "kind": "Integer", "id" : "INTEGER" }
BOOL = { "kind": "Bool", "id": "BOOL" }

def make_enumerated(name):
    """Creates an AST node for an element of an enumerated set.
    The created node does not have the type attribute set."""
    return { "kind": "Enumerated", "id": name }

def make_enumeration(name, elements):
    """Creates an AST node for an enumerated set. Sets the
    type nodes for all the elements to the created node."""
    assert type(elements) is list
    n = { "kind": "Enumeration", "id": name, "elements": elements }
    for e in elements:
        e["type"] = n
    return n

### TERMINALS ###

def make_const(name, typedef, value):
    """Creates an AST node for a B constant."""
    return { "kind": "Cons", "id": name, "type": typedef, "value" : value }


def make_var(name, typedef, scope):
    """Creates an AST node for a B variable."""
    return { "kind": "Vari", "id": name, "type": typedef, "scope": scope }

def make_imp_var(name, typedef):
    """Creates an AST node for a B concrete variable."""
    return make_var(name, typedef, "Impl")

def make_loc_var(name, typedef):
    """Creates an AST node for a B local variable."""
    return make_var(name, typedef, "Local")

def make_arg_var(name, typedef):
    """Creates an AST node for a B operation argument."""
    return make_var(name, typedef, "Oper")

def make_intlit(value):
    '''
    Creates an AST node for a B integer literal.

    - Input: an integer value

    '''
    if value == "MAXINT":
        return MAXINT
    if value == "MININT":
        return MININT
    return { "kind": "IntegerLit",
             "value": str(value)}

def make_boollit(value):
    '''
    Creates an AST node for a Boolean literal.

    - Input: "FALSE" or "TRUE"

    '''
    return { "kind": "BooleanLit",
             "value": value}

FALSE = make_boollit("FALSE")
TRUE = make_boollit("TRUE")

ZERO = make_intlit(0)
ONE = make_intlit(1)
# These range are specified in Atelier B legacy configurations
MAXINT = make_intlit(2147483647)
MININT = make_intlit(-2147483647)
# These range are specified in LLVM as INT32_MAX 
#MININT = make_intlit(-MAXINT-1)

def make_sset(node):
    """Creates an AST node for a B simple set."""
    res = None
    if (node.get("op") == ".."):
        res = make_interval( make_intlit(node[0].get("value")),make_intlit(node[1].get("value")))
    elif (node.get("op") == "*"):
        res = become_list(make_sset(node[0]))
        res += become_list(make_sset(node[1]))
    elif (node.get("value") == INTEGER["id"] or node.get("value") == "INT"):
        res = make_interval(MININT.get("value"),MAXINT.get("value"))
    elif (node.get("value") == NATURAL["id"] or node.get("value") == "NAT"):
        res = make_interval(ZERO.get("value"),MAXINT.get("value"))
    elif (node.get("value") == BOOL["id"] or node.get("value") == "BOOL"):
        res = make_interval(ZERO.get("value"),ONE.get("value"))

    if res == None:
        print("warning: the value "+ (node.get("value"))+ " is not visible in this version.")
        print("warning: simple set not supported.")
        exit(1)
    return res

def become_list(elem):
    """Creates a list and avoid to create sublist in list."""
    if (type( elem ) == list ):
        return elem
    else: return  [elem]

def make_structure(fields):
    """Creates an AST node for a B Structure."""
    assert type(fields) is list and fields[0]["kind"] =="Field"
    return {"kind" : "Structure" , "fields" : fields}

def make_field(id_,type_):
    """Creates an AST node for a B Field."""
    assert type(id_)==str
    return {"kind" : "Field", "id" : id_,"type" : type_}

def make_record(type_,values):
    """Creates an AST node for a B Record."""
    assert type(values) is list
    return {"kind" : "Record", "type" : type_,"values" : values}

def make_recfield(base_,id_):
    """Creates an AST node for a B RecField."""
    #TODO: To support nested record to change base_["kind"] == "Record"
    assert base_["kind"] == "Vari" and base_["type"]["kind"] =="Structure" and type(id_)==str
    return {"kind" : "RecField", "base" : base_,"id" : id_}

def make_interval(start,end):
    """Creates an AST node for a B simple interval set."""
    return { "kind" : "Interval" , "start" : start, "end" : end }

def make_array_type(domxml, ranxml):
    """Creates an AST node for a B ArrayType."""
    #TODO: create support to INT type and NAT
    #TODO: support a list of indices in domain
    dom = become_list(make_sset(domxml))
    ran = make_sset(ranxml)
    assert dom != None and ran != None
    return { "kind" : "ArrayType", "dom": dom, "ran" : ran}

def make_array_item(base,indexes):
    """Creates an AST node for a B ArrayItem."""
    return { "base": base, "indexes": indexes,
            "kind":"ArrayItem", "type": INTEGER}

def make_array_cproduct(arg1, arg2):
    """Creates an AST node for a B Array.
        - Input:
            arg1 is seq interval
            arg2 is Literal
        - Example:     0..10*0..10*0..10*{1}

    """
    assert type(arg1) is list
    assert type(arg2) is not list
    return {  "kind":"CProduct" , "arg1": arg1,"arg2": arg2}


def make_set(arg1,arg2=None):
    """Creates an AST node for a B Array.
        - Example: {10}                                     (Set)
        - Example: { ((0|->0)|->0)|->10,((0|->0)|->1)|->10} (Map)
    """
    if arg2==None:
        assert type(arg1) is list
        return {  "kind":"Set" , "args": arg1}
    else:
        return make_map(arg1,arg2)


def make_map(arg1,arg2):
    """Creates an AST node for a B Map."""
    return { "arg1": arg1, "arg2": arg2,
            "kind":"Map", "type": INTEGER}

### COMPOSED EXPRESSIONS ###

def make_term(operator, args):
    """Creates an AST node for a B term."""
    return { "kind": "Term", "op": operator, "args" : args }

def make_succ(term):
    """Creates an AST node for a B succ (successor) expression."""
    return make_term("succ", [term])

def make_pred(term):
    """Creates an AST node for a B pred (predecessor) expression."""
    return make_term("pred", [term])

def make_sum(term1, term2):
    """Creates an AST node for a B sum expression."""
    return make_term("+", [term1, term2])

def make_diff(term1, term2):
    """Creates an AST node for a B subtraction expression."""
    return make_term("-", [term1, term2])

def make_prod(t1, t2):
    """Creates an AST node for a B product expression."""
    # We consider that Cartesian Product is not associative then
    # always the first element is a Term in the final of Cartesian Product
    kindLit = ["ArrayItem","IntegerLit","BooleanLit", "Term", "Vari", "Cons", "RecField"] # Integer Multiplication
    term1, term2 = t1, t2

    if term1["kind"] in kindLit and term2["kind"] in kindLit:
            return make_term("*", [term1, term2])
    elif term1["kind"] == "Interval" and term2["kind"] == "Interval":
            return make_array_cproduct([term1], term2)
    for i in range(2):
        if term1["kind"] == "Interval" and term2["kind"] in kindLit:
            return make_array_cproduct([term1], term2)
        elif term1["kind"] == "CProduct" and term2["kind"] in kindLit:
            return make_array_cproduct(term1["arg1"]+[term1["arg2"]], term2)
        elif term1["kind"] == "CProduct" and term2["kind"] == "Interval":
            lit, set = (term1["arg2"], term2) if term1["arg2"]["kind"] in kindLit  else (term2 , term1["arg2"])
            return make_array_cproduct(term1["arg1"]+[set], lit)
        term2, term1 = t1, t2

    assert False, "Unsupported Cartesian Product. It is not in B0:"+printer.expression(term1)+"*"+printer.expression(term1)


def make_div(term1, term2):
    """Creates an AST node for a B division expression."""
    return make_term("/", [term1, term2])


def make_mod(term1, term2):
    """Creates an AST node for a B mod expression."""
    return make_term("mod", [term1, term2])

def make_comp(operator, arg1, arg2):
    """Creates an AST node for a B comparison."""
    return { "kind": "Comp", "op": operator, "arg1": arg1, "arg2": arg2 }

def make_le(term1, term2):
    """Creates an AST node for a B "lower or equal" expression."""
    return make_comp("<=", term1, term2)

def make_lt(term1, term2):
    """Creates an AST node for a B "lower than" expression."""
    return make_comp("<", term1, term2)

def make_ge(term1, term2):
    """Creates an AST node for a B "greater or equal" expression."""
    return make_comp(">=", term1, term2)

def make_gt(term1, term2):
    """Creates an AST node for a B "greater than" expression."""
    return make_comp(">", term1, term2)

def make_eq(term1, term2):
    """Creates an AST node for a B equality."""
    return make_comp("=", term1, term2)

def make_neq(term1, term2):
    """Creates an AST node for a B inequality."""
    return make_comp("!=", term1, term2)

def make_form(operator, args):
    """Creates an AST node for a B formula."""
    return { "kind": "Form", "op": operator, "args" : args }

def make_and(arg1, arg2):
    """Creates an AST node for a B conjunction."""
    return make_form("and", [arg1, arg2])

def make_or(arg1, arg2):
    """Creates an AST node for a B disjunction."""
    return make_form("or", [arg1, arg2])

def make_not(arg):
    """Creates an AST node for a B negation."""
    return make_form("not", [arg])

### INSTRUCTIONS ###

def make_skip():
    """Creates an AST node for a B skip instruction."""
    return { "kind": "Skip" }

def make_beq(lhs, rhs):
    """Creates an AST node for a B becomes equal instruction."""
    return { "kind": "Beq", "lhs": lhs, "rhs": rhs}

def make_bin(lhs):
    """Represents a becomes in susbtitution (but only the left hand side)."""
    return { "kind": "Bin", "lhs": lhs}

def make_blk(body):
    """Creates an AST node for a B block instruction."""
    assert type(body) is list
    return { "kind": "Blk", "body": body }

def make_var_decl(variables, body):
    """Creates an AST node for a B variable declaration instruction."""
    assert type(variables) is list
    assert type(body) is list
    return { "kind": "VarD", "vars": variables, "body": body }

def make_if_br(cond, body):
    """Creates an AST node for a B if branch."""
    assert type(body) is not list
    return { "kind": "IfBr", "cond": cond, "body": body}

def make_if(branches):
    """Creates an AST node for a B if instruction."""
    assert type(branches) is list
    return { "kind": "If", "branches": branches }

def make_case_br(value, body):
    """Creates an AST node for a B case branch."""
    #assert type(value) is list
    assert type(body) is not list
    return { "kind": "CaseBr", "val": value, "body": body }

def make_case(expression, branches):
    """Creates an AST node for a B case instruction."""
    assert type(branches) is list
    return { "kind": "Case", "expr": expression, "branches": branches }

def make_while(cond, body):
    """Creates an AST node for a B while instruction."""
    assert type(body) is list
    return { "kind": "While", "cond": cond, "body": body }

def make_call(operator, inp, out, inst=None):
    '''
    Creates an AST node for a B operation call instruction.

    - Input:
    op: an operation
    inp: a sequence of inputs
    out: a sequence of outputs
    inst: an element of an imported clause (optional)

    '''
    return { "kind": "Call", "op": operator, "inp": inp, "out": out,
             "inst": inst }

### COMPONENT ###

def make_oper(name, inp, out, body):
    """Creates an AST node to represent a B operation."""
    assert type(inp) is list
    assert type(out) is list
    assert type(body) is not list
    return { "kind": "Oper", "id": name, "inp": inp, "out": out, "body": body }

def make_import(mach, prefix = None):
    '''
    Creates an AST node for a B import element.

    - Input:
    mach: a machine or a library machine
    prefix: a string prefix (optional).
    - Output:
    Creates an element of an import clause, i.e., a module.
    '''
    assert(mach["kind"] in { "Machine" })
    return { "kind": "Module", "mach": mach, "pre": prefix }

def make_implementation(name, imports, sets, consts, variables, init, ops):
    """Creates an AST node for a B implementation module."""
    assert type(imports) is list
    assert type(consts) is list
    assert type(sets) is list
    assert type(variables) is list
    assert type(init) is list
    assert type(ops) is list
    root = { "kind": "Impl",
             "id": name,
             "machine": None,
             "imports": imports,
             "sets": sets,
             "concrete_constants": consts,
             "variables": variables,
             "initialisation": init, "operations": ops,
             "stateful": None}
    for node in imports + variables + ops:
        node["root"] = root
    return root

def make_base_machine(name, sets, consts, variables, ops):
    '''
    Constructor for node that represent a machine interface, namely
    the elements of a machine that may be accessed by a module depending
    on that machine.
    '''
    assert type(sets) is list
    assert type(consts) is list
    assert type(variables) is list
    assert type(ops) is list
    root = { "kind": "Machine",
             "id": name,
             "base": True,
             "concrete_constants": consts,
             "variables": variables,
             "operations": ops,
             "implementation": None,
             "stateful": None,
             "comp_direct": None,
             "comp_indirect": None}
    for node in variables + ops:
        node["root"] = root
    return root

def make_developed_machine(name, sets, impl):
    '''
    Constructor for node that represents a developed machine.

    For now we are just interested in the implementation of that machine.
    '''
    assert type(sets) is list
    return { "kind": "Machine",
             "id": name,
             "base": False,
             "sets": sets,
             "concrete_constants": [],
             "variables": [],
             "implementation": impl,
             "stateful": None,
             "comp_direct": None,
             "comp_indirect": None
             }

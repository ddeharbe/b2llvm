#ifndef _ID00068llvm_h
#define _ID00068llvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {bool vari_bool }   COMPREF$state$;
typedef COMPREF$state$ * COMPREF$ref$;
extern void COMPREF$init$(COMPREF$ref$ self);
#endif /* _ID00068llvm_h */

#ifndef _COMPIMP_h
#define _COMPIMP_h

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Clause SETS */

/* Clause CONCRETE_VARIABLES */


/* Clause CONCRETE_CONSTANTS */
/* Basic constants */
/* Array and record constants */
extern void COMPIMP__INITIALISATION(void);

/* Clause OPERATIONS */

extern void COMPIMP__do(void);
extern void COMPIMP__do_int(long value, long *res);
extern void COMPIMP__do_string(const char* value, long *res);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _COMPIMP_h */

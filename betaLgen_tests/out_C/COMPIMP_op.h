#ifndef _COMPIMP_op_h
#define _COMPIMP_op_h

/* Clause IMPORTS */
#include "COMPIMP.h"
#define COMPIMP_op__do COMPIMP__do

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Clause SETS */

/* Clause CONCRETE_VARIABLES */

extern long COMPIMP_op__var;

/* Clause CONCRETE_CONSTANTS */
/* Basic constants */
/* Array and record constants */
extern void COMPIMP_op__INITIALISATION(void);

/* Clause OPERATIONS */


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _COMPIMP_op_h */

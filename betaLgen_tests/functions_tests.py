import os
import subprocess
import collections
import time
# Basic functions
def filename():
    name = "ID"+"{:0>5d}".format(filename.count_name)
    filename.count_name += 1
    return name
filename.count_name = 0



#Declare functions
dic = collections.OrderedDict()
def add_dic(i,j):
    dic[i]=j


frequency_status_code = collections.OrderedDict()
def add_freq_status_code(i):
    if i in frequency_status_code:
        frequency_status_code[i]= frequency_status_code[i]+1
    else:
        frequency_status_code[i] = 1




def print_example_title(name):
    print("*****************************")
    print("** "+ name)
    print("*****************************")

def replace_all(fullText):
    for i, j in dic.items():
        if( i in fullText):
            fullText = fullText.replace(i, j)
        else:
            print("ERROR: text not found to be replaced:"+i )
    return fullText


def execute(command, n, test):
    tmp = executeSub(command,n)
    add_freq_status_code(str(tmp)+" "+test)
    return tmp
execute.oks = 0

def print_out(a):
    #print(str(a, "utf-8"))
    print(str(a))

def execute_time(cmd, n="" , out=True):
    start_time = time.time()
    res = executeSub(cmd, n , out)
    if res==0:
        print("Approved "+n)
    else:
        print("Error reported in "+n+" and the return code is "+str(res))
    print("Time: %.2f s"%(time.time() - start_time))
    
    return res,(time.time() - start_time)

def executeSub(cmd, n="" , out=True):
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    output, errors = p.communicate()
    if out:
        if p.returncode==0:
            print("Approved "+n)
            print_out(output)
            
        else:
            print("Error reported in "+n+" and the return code is "+str(p.returncode))
            print_out(output)
            print_out(errors)
        
    return p.returncode

comp_extras = []
def IsUseful(n):
    add_dic(n,n)
    comp_extras.append(n)



def fun_comp_extras(imps, pProject):
    i = "COMPREF_i"
    command = './b2c  -I ' + pProject + ' -C ./out_C '+pProject+i+'.imp '
    print(command)
    compref_OK = 1 - execute(command, i, "")
    imps.append("COMPREF_i")
    return compref_OK


def printing(mch, imp):
    return """
<developed>
 <machine>"""+mch+"""</machine>
 <implementation>"""+imp+"""</implementation>
</developed>

"""


def creating_B_project(nTests):
    for i in range(0,nTests):
        id_test = filename()
        project = "<project>"
        project+=printing("COMPREF",id_test)

        project+="""
    <base>
      <machine>COMPIMP</machine>
    </base>
    <base>
      <machine>COMPIMP_INT</machine>
    </base>
    <base>
      <machine>COMPIMP_BOOL</machine>      
    </base>
"""
        project += "</project>"
        f = open('./out/project_'+id_test+'.xml','w')
        f.write(project)
    return 


def calling_b2llvm(pProject = "./out/",coverage=True):
    #TODO: to clean the coverage
    COV_RUN="coverage run --source=b2llvm.py,b2llvm -p"
    if pProject == "./out/":
        b2llvm_path =" ../b2llvm.py"
    else:
        b2llvm_path =" ./b2llvm.py"
    files = os.listdir(pProject)
    approved_grammar_tests=[]
    unsupported_grammar_tests=[]
    failed_grammar_tests=[]
    for file in files :
        if file.startswith("ID",0) :
            file = file[:-5]#remove the extension
            print_example_title(file)
            # Calling b2llvm by machine (old format)
            #cmd = COV_RUN +" ../b2llvm.py -m comp COMPREF "+file+".llvm "+pProject+" project_"+file+".xml"
            cmd = COV_RUN +b2llvm_path+" -m comp "+file+" "+file+".llvm "+pProject+" project_"+file+".xml"
            
            cmds = cmd.split(" ")
            cmds[0] = "/usr/local/bin/coverage" # Used in OS X
            #cmds[0] = "python3-coverage"
            #cmds[0] = "/usr/bin/python"
            #print(cmds)
            if not(coverage):
                cmds=cmds[4:]
            print(' '.join(cmds))
            sp = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = sp.communicate()
 
            if sp.returncode==0:
                approved_grammar_tests.append(file)
                print("Approved")
            elif sp.returncode == 65:
                unsupported_grammar_tests.append(file)
                print("Unsupported")
            else:
                failed_grammar_tests.append(file)
                if out:
                    print ("standard output of subprocess:\n")
                    print(str(out,"utf-8"))
                if err:
                    print ("standard error of subprocess:\n")
                    print(str(err,"utf-8"))
            print ("return code of subprocess:%d" %sp.returncode)

    print("Approved tests (%d), failed tests (%d), unsupported tests (%d) and total tests (%d)" %(len(approved_grammar_tests),len(failed_grammar_tests),len(unsupported_grammar_tests),len(approved_grammar_tests+failed_grammar_tests+unsupported_grammar_tests)))
    print("***** Approveds (%d) ****" %(len(approved_grammar_tests)))
    for i in approved_grammar_tests:
        print(i)
    print("***** Unsupported tests (%d) ****" %(len(unsupported_grammar_tests)))
    for i in unsupported_grammar_tests:
        print(i)
    print("***** Faileds (%d) ****" %(len(failed_grammar_tests)))
    for i in failed_grammar_tests:
        print(i)
    return approved_grammar_tests


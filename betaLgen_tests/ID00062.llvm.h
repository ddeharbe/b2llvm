#ifndef _ID00062llvm_h
#define _ID00062llvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {bool vari_bool }   COMPREF$state$;
typedef COMPREF$state$ * COMPREF$ref$;
extern void COMPREF$init$(COMPREF$ref$ self);
#endif /* _ID00062llvm_h */

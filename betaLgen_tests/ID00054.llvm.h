#ifndef _ID00054llvm_h
#define _ID00054llvm_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t vari_int }   COMPREF$state$;
typedef COMPREF$state$ * COMPREF$ref$;
extern void COMPREF$init$(COMPREF$ref$ self);
#endif /* _ID00054llvm_h */

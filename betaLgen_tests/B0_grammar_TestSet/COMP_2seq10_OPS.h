#ifndef _COMP_2seq10_OPS_h
#define _COMP_2seq10_OPS_h

#include <stdint.h>
#include <stdbool.h>
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Clause SETS */

/* Clause CONCRETE_VARIABLES */


/* Clause CONCRETE_CONSTANTS */
/* Basic constants */
/* Array and record constants */
extern void COMP_2seq10_OPS__INITIALISATION(void);

/* Clause OPERATIONS */

extern void COMP_2seq10_OPS__ID00000(void);
extern void COMP_2seq10_OPS__ID00001(void);
extern void COMP_2seq10_OPS__ID00002(void);
extern void COMP_2seq10_OPS__ID00003(void);
extern void COMP_2seq10_OPS__ID00004(void);
extern void COMP_2seq10_OPS__ID00005(void);
extern void COMP_2seq10_OPS__ID00006(void);
extern void COMP_2seq10_OPS__ID00007(void);
extern void COMP_2seq10_OPS__ID00008(void);
extern void COMP_2seq10_OPS__ID00009(void);
extern void COMP_2seq10_OPS__ID00010(void);
extern void COMP_2seq10_OPS__ID00011(void);
extern void COMP_2seq10_OPS__ID00012(void);
extern void COMP_2seq10_OPS__ID00013(void);
extern void COMP_2seq10_OPS__ID00014(void);
extern void COMP_2seq10_OPS__ID00015(void);
extern void COMP_2seq10_OPS__ID00016(void);
extern void COMP_2seq10_OPS__ID00017(void);
extern void COMP_2seq10_OPS__ID00018(void);
extern void COMP_2seq10_OPS__ID00019(void);
extern void COMP_2seq10_OPS__ID00020(void);
extern void COMP_2seq10_OPS__ID00021(void);
extern void COMP_2seq10_OPS__ID00022(void);
extern void COMP_2seq10_OPS__ID00023(void);
extern void COMP_2seq10_OPS__ID00024(void);
extern void COMP_2seq10_OPS__ID00025(void);
extern void COMP_2seq10_OPS__ID00026(void);
extern void COMP_2seq10_OPS__ID00027(void);
extern void COMP_2seq10_OPS__ID00028(void);
extern void COMP_2seq10_OPS__ID00029(void);
extern void COMP_2seq10_OPS__ID00030(void);
extern void COMP_2seq10_OPS__ID00031(void);
extern void COMP_2seq10_OPS__ID00032(void);
extern void COMP_2seq10_OPS__ID00033(void);
extern void COMP_2seq10_OPS__ID00034(void);
extern void COMP_2seq10_OPS__ID00035(void);
extern void COMP_2seq10_OPS__ID00036(void);
extern void COMP_2seq10_OPS__ID00037(void);
extern void COMP_2seq10_OPS__ID00038(void);
extern void COMP_2seq10_OPS__ID00039(void);
extern void COMP_2seq10_OPS__ID00040(void);
extern void COMP_2seq10_OPS__ID00041(void);
extern void COMP_2seq10_OPS__ID00042(void);
extern void COMP_2seq10_OPS__ID00043(void);
extern void COMP_2seq10_OPS__ID00044(void);
extern void COMP_2seq10_OPS__ID00045(void);
extern void COMP_2seq10_OPS__ID00046(void);
extern void COMP_2seq10_OPS__ID00047(void);
extern void COMP_2seq10_OPS__ID00048(void);
extern void COMP_2seq10_OPS__ID00049(void);
extern void COMP_2seq10_OPS__ID00050(void);
extern void COMP_2seq10_OPS__ID00051(void);
extern void COMP_2seq10_OPS__ID00052(void);
extern void COMP_2seq10_OPS__ID00053(void);
extern void COMP_2seq10_OPS__ID00054(void);
extern void COMP_2seq10_OPS__ID00055(void);
extern void COMP_2seq10_OPS__ID00056(void);
extern void COMP_2seq10_OPS__ID00057(void);
extern void COMP_2seq10_OPS__ID00058(void);
extern void COMP_2seq10_OPS__ID00059(void);
extern void COMP_2seq10_OPS__ID00060(void);
extern void COMP_2seq10_OPS__ID00061(void);
extern void COMP_2seq10_OPS__ID00062(void);
extern void COMP_2seq10_OPS__ID00063(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* _COMP_2seq10_OPS_h */

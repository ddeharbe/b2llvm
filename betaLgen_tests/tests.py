import os
import sys
import subprocess
import collections
import functions_tests as f


#Clean the screen
if "clean" in sys.argv:
    print ("\n"*10)
    os.system("rm ./out/ID*.bxml")
    os.system("rm ./out_C/*.c")
    os.system("rm *.llvm.h")
    os.system("rm ./out_C/*.llvm")
    os.system("rm *.llvm *.o *.s")
    print("OK - Cleaned the files (bxml,c,llvm)")
    sys.exit()


print("*****************************************************************")
print("* 1.0 Starting the generator of tests:                          *")
print("*****************************************************************")
myfile = open("B0_grammar_TestSet/b0_grammar_full.productionCoverage.1.11.out")
fullText = myfile.read()
f.execute.oks = 0


print("*****************************************************************")
print("* 2.0 Making the replaces                                       *")
print("*****************************************************************")

#1. Removing the not useful and valid tests ( removing examples with erros)
##1.1 There is a equivalent example and these example only changed the VARIANT CLAUSE, but this clause should be a integer. This problem is a semantic error.
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID INITIALISATION WHILE ID = ID DO skip INVARIANT INTLit = INTLit VARIANT TRUE END END\n","")
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID INITIALISATION WHILE ID = ID DO skip INVARIANT INTLit = INTLit VARIANT FALSE END END\n","")
##1.2 The IMPORT CLAUSE not support passing a register as parameter. This problem is a restriction from C4B and B2LLVM. This restriction happens to avoid the use of pointers in generated code (according to EN50128/IEC61508 standard, pointers have to be used moderately)
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID IMPORTS ID ID ( rec ( ID : ID ) ) END\n","") #ID00023
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID IMPORTS ID ID ( rec ( ID : { ID |-> MAXINT } ) ) END\n","")
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID IMPORTS ID ID ( rec ( ID : ID * { MAXINT } ) ) END\n","")
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID IMPORTS ID ID ( rec ( ID : ID ) ' ID ) END\n","")
## 1.3 There is no sense define an integer like a boolean (False and True).  This problem is a semantic error.
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID PROPERTIES INTLit = FALSE END\n","") #ID00040
f.add_dic("IMPLEMENTATION ID ( ID ) REFINES ID PROPERTIES INTLit = TRUE END\n", "") #ID00041

#2. Replacing some identifiers.
f.add_dic("IMPLEMENTATION ID ", "IMPLEMENTATION ID1 ")
f.add_dic("ID . ID", "ID.ID")
f.add_dic("SEES ID", "SEES COMPSEES")
f.add_dic("REFINES ID", "REFINES COMPREF_r")
f.add_dic("IMPORTS ID ID", "IMPORTS xxx.COMPIMP")
f.add_dic("INTLit", "0")
f.add_dic("CString", "\"Hello\"")
##2.1 Replacing a identifier and initializing the variable.
f.add_dic("CONCRETE_VARIABLES ID ", "CONCRETE_VARIABLES ID INITIALISATION ID :=1 ")

#3. Fixing the type of imported component. The solution is importing a component with the parameter of the same type. This problem is a semantic error.
f.add_dic("COMPIMP ( 0 ) ", "COMPIMP_INT ( 0 ) ")
f.add_dic("COMPIMP ( MAXINT ) ", "COMPIMP_INT ( 32767 ) ")
f.add_dic("COMPIMP ( MININT ) ", "COMPIMP_INT ( -32768 ) ")
f.add_dic("COMPIMP ( FALSE ) ", "COMPIMP_BOOL ( FALSE ) ")
f.add_dic("COMPIMP ( TRUE ) ", "COMPIMP_BOOL ( TRUE ) ")
##3.1 Fixing the type of imported component and declaring the used STRUCT.
##ID0008
dec_rec ="CONCRETE_CONSTANTS const_rec PROPERTIES const_rec: struct( aa : INT) VALUES  const_rec =  rec( aa : 1) "
imp_rec = "REFINES COMPREF_r  IMPORTS xxx.COMPIMP_INT ( const_rec ' aa )"
f.add_dic("REFINES COMPREF_r IMPORTS xxx.COMPIMP ( ID ' ID ) ", dec_rec+imp_rec)
#ID0012
dec_rec ="CONCRETE_CONSTANTS const_rec, vec PROPERTIES const_rec: struct( aa : INT) &  vec = { 0|-> 0, 1|->1} VALUES  const_rec =  rec( aa : 1) ; vec = { 0|-> 0, 1|->1} "
imp_rec = "REFINES COMPREF_r  IMPORTS xxx.COMPIMP_INT (vec ( const_rec ' aa ))"
f.add_dic("REFINES COMPREF_r IMPORTS xxx.COMPIMP ( ID ( ID ' ID ) ) ", dec_rec+imp_rec)
##3.2 Fixing the type of imported component (int).
f.add_dic("IMPORTS xxx.COMPIMP ( 0 + 0 )","IMPORTS xxx.COMPIMP_INT ( 0 + 0 )")
f.add_dic("IMPORTS xxx.COMPIMP ( 0 - 0 )","IMPORTS xxx.COMPIMP_INT ( 0 - 0 )")
f.add_dic("IMPORTS xxx.COMPIMP ( - 0 )","IMPORTS xxx.COMPIMP_INT ( - 0 )")
f.add_dic("IMPORTS xxx.COMPIMP ( 0 * 0 )","IMPORTS xxx.COMPIMP_INT ( 0 * 0 )")
f.add_dic("IMPORTS xxx.COMPIMP ( 0 / 0 )","IMPORTS xxx.COMPIMP_INT ( 0 / 0 )")
f.add_dic("IMPORTS xxx.COMPIMP ( 0 mod 0 )","IMPORTS xxx.COMPIMP_INT ( 0 mod 0 )")
f.add_dic("IMPORTS xxx.COMPIMP ( 0 ** 0 )","IMPORTS xxx.COMPIMP_INT ( 0 ** 0 )")
f.add_dic("IMPORTS xxx.COMPIMP ( succ ( 0 ) )","IMPORTS xxx.COMPIMP_INT ( succ ( 0 ) )")
f.add_dic("IMPORTS xxx.COMPIMP ( pred ( 0 ) )","IMPORTS xxx.COMPIMP_INT ( pred ( 0 ) )")
f.add_dic("IMPORTS xxx.COMPIMP ( ( 0 ) )","IMPORTS xxx.COMPIMP_INT ( ( 0 ) )")
##3.3 Fixing the type of imported component and declaring the used VECTOR.
infValue = "CONCRETE_CONSTANTS  vec  PROPERTIES     vec = { 0|-> 0, 1|->1} /* type */ VALUES     vec = { 0|-> 0, 1|->1}  /* value */"
orig = "REFINES COMPREF_r IMPORTS xxx.COMPIMP ( ID ( ID ) ) END"
new = "REFINES COMPREF_r IMPORTS xxx.COMPIMP_INT ( vec(0) ) "+  infValue + " END"
f.add_dic(orig , new)
##3.4 Fixing the type of promoted component and declaring the respective initialisation.
f.add_dic("REFINES COMPREF_r PROMOTES ID END", "REFINES COMPIMP_op IMPORTS COMPIMP(NAT) PROMOTES do INITIALISATION var:= 0 END")
##3.5 Fixing the type of extended component.
f.add_dic("REFINES COMPREF_r EXTENDS ID.ID ( ID ) END","REFINES COMPREF_r EXTENDS xxx.COMPIMP_INT ( 0 ) END")

#4. Declaring the identifiers (Sets/constants/values) and defining them.
f.add_dic("SETS ID END" , "SETS simplerange VALUES simplerange = 0..3 END")
f.add_dic("SETS ID = { ID } END" , "SETS simpleset = { a0 } END")
f.add_dic("CONCRETE_CONSTANTS ID END" , "CONCRETE_CONSTANTS simpleconstant PROPERTIES simpleconstant : NAT VALUES simpleconstant = 1  END")
f.add_dic("CONSTANTS ID END" , "CONSTANTS simpleconstant PROPERTIES simpleconstant : NAT VALUES simpleconstant = 1  END")
f.add_dic("VALUES ID = ID", "CONSTANTS simpleconstant PROPERTIES simpleconstant : INT  VALUES simpleconstant = MAXINT")
f.add_dic("VALUES ID = Bool ( ID = ID )","CONSTANTS simplebool PROPERTIES simplebool : BOOL VALUES simplebool = bool ( MAXINT = MAXINT )")
f.add_dic("VALUES ID = Bool ( ID /= ID )","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool = bool ( MAXINT /= MAXINT )")
f.add_dic("VALUES ID = Bool ( ID /= ID )","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool = bool ( MAXINT /= MAXINT )")
f.add_dic("VALUES ID = Bool ( ID < ID )","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool = bool ( MAXINT < MAXINT )")
f.add_dic("VALUES ID = Bool ( ID > ID )","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool = bool ( MAXINT > MAXINT )")
f.add_dic("VALUES ID = Bool ( ID <= ID )","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool = bool ( MAXINT <= MAXINT )")
f.add_dic("VALUES ID = Bool ( ID >= ID )","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool = bool ( MAXINT >= MAXINT )")
f.add_dic("VALUES ID = Bool ( ID = ID & ID = ID ) ","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool =  bool ( MAXINT = MAXINT & MININT = MININT )")
f.add_dic("VALUES ID = Bool ( ID = ID or ID = ID ) ","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool =  bool ( MAXINT = MAXINT or MININT = MININT )")
f.add_dic("VALUES ID = Bool ( not ( ID = ID ) ) ","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool =  bool ( not ( 0 = 1 ))")
f.add_dic("VALUES ID = Bool ( ( ID = ID ) ) ","CONSTANTS simplebool PROPERTIES simplebool : BOOL  VALUES simplebool =  bool ( not ( 0 = 1 ))")
f.add_dic("VALUES ID = 0 .. 0 ","CONSTANTS simpleset PROPERTIES simpleset <: NAT  VALUES simpleset = 0 .. 0 ")
f.add_dic("CONCRETE_VARIABLES ID INITIALISATION ID :=1 ", "CONCRETE_VARIABLES vari INVARIANT vari : INT INITIALISATION vari :=1 ")
#4.1 Declaring the initialization for respective variables used before
f.add_dic("INITIALISATION BEGIN ID ( MAXINT ) := MAXINT END","CONCRETE_VARIABLES vari_array INVARIANT vari_array : 0..MAXINT --> NAT INITIALISATION BEGIN vari_array ( MAXINT ) := MAXINT END")
f.add_dic("INITIALISATION BEGIN ID := ID END","CONCRETE_VARIABLES vari_int INVARIANT vari_int : INT INITIALISATION BEGIN vari_int := 1 END")
f.add_dic("INITIALISATION BEGIN ID ' ID := MAXINT", "CONCRETE_VARIABLES var_rec INVARIANT var_rec : struct( aa : INT) INITIALISATION var_rec:= rec( aa : 1); BEGIN var_rec ' aa := MAXINT", )
#4.2 Declaring the IMPORT, VARIABLES and INITIALISATION clauses to call an imported operation.
## These examples call imported operations, but the imported machines, variables, constants are not previously declared or initialized.
f.add_dic("INITIALISATION BEGIN ID <-- ID ( MAXINT ) END", "IMPORTS COMPIMP(ID) CONCRETE_VARIABLES vari_int INVARIANT vari_int : INT INITIALISATION  BEGIN  vari_int <-- do_int ( MAXINT ) END")
f.add_dic("INITIALISATION BEGIN ID <-- ID ( \"Hello\" ) END END","IMPORTS COMPIMP(ID) CONCRETE_VARIABLES vari_int INVARIANT vari_int : INT INITIALISATION  BEGIN vari_int <-- do_string ( \"Hello\" ) END END")
f.add_dic("VAR ID IN skip END", "VAR var_temp IN var_temp:=1 END")
f.add_dic("INITIALISATION ID := bool ( ID = ID )", "CONCRETE_VARIABLES vari_bool INVARIANT vari_bool : BOOL INITIALISATION vari_bool := bool ( 0 = 0 )")
f.add_dic("REFINES COMPREF_r OPERATIONS ID <-- ID ( ID ) = BEGIN skip END", "REFINES COMPIMP OPERATIONS do = BEGIN skip END; res <-- do_int (value)  =  BEGIN res := value + 0 END ; res <-- do_string (value)  = BEGIN res := 0 END")
f.add_dic("INITIALISATION CASE ID OF EITHER ID THEN skip  ELSE skip END","CONCRETE_VARIABLES vari_bool INVARIANT vari_bool : BOOL INITIALISATION vari_bool:= FALSE; CASE vari_bool OF EITHER TRUE THEN skip  ELSE skip END")
f.add_dic("INITIALISATION CASE ID OF EITHER ID THEN skip OR ID THEN skip ELSE skip END","CONCRETE_VARIABLES vari_bool INVARIANT vari_bool : BOOL INITIALISATION vari_bool:= FALSE; CASE vari_bool OF EITHER TRUE THEN skip OR FALSE THEN skip ELSE skip END")



global comp_extras
comp_extras = []


# Checking if the modules are used
#f.IsUseful("COMP_OPS_IMP")
#f.IsUseful("COMP_OPS")
f.IsUseful("COMPIMP_BOOL")
f.IsUseful("COMPIMP_INT")
f.IsUseful("COMPIMP_op")
#f.IsUseful("COMPIMP_REC")
f.IsUseful("COMPIMP")
#f.IsUseful("COMPREF_i")
f.IsUseful("COMPREF_r")
#f.IsUseful("COMPREF_REC")
f.IsUseful("COMPREF")
f.IsUseful("COMPSEES")





fullText = f.replace_all(fullText)



print("*****************************************************************")
print("* 3.0 Generating the new file tests                             *")
print("*****************************************************************")
myfile = open("B0_grammar_TestSet/GeneratedTestsFull.txt","w")
myfile.write(fullText)
myfile.close()
j=0
#3.1 Print in several files
setOfTest = fullText.split("\n")
for i in setOfTest:
    j+=1
    print(j)
    print(i)
#setOfTest.pop(-1)#Remove the last element that is empty

nTests = len(setOfTest)
for c in range(0,nTests):
    n = f.filename()
    #replace the name of file
    setOfTest[c]=setOfTest[c].replace("ID1",n)
    #print(   setOfTest[c])
    myfile = open("B0_grammar_TestSet/"+n+".imp","w")
    myfile.write(setOfTest[c])
    myfile.close()


print("*****************************************************************")
print("* 4.0 Calling the BXML                                          *")
print("*****************************************************************")
pBXML ="/Applications/AtelierB_4.3/AtelierB.app/AB/bbin/macosx/bxml"
#pBXML = "/Applications/AtelierB.app/AB/bbin/macosx/bxml"
#pBXML = "./bxml"
pProject = "./B0_grammar_TestSet/"
pOut = "./out/"

f.filename.count_name=0
f.execute.oks = 0
for test in setOfTest:
    n = f.filename()
    command = pBXML+' -c  -i4 -I '+ pProject + ' -O'+pOut+' '+pProject+n+'.imp '
    print(command)
    f.execute(command,n," with B0Checker")
    f.execute(command.replace(" -c  "," -a "),n,"only adding the type information")

name = 'COMPREF.mch '
command = pBXML+' -c  -i4 -I '+ pProject + ' -O'+pOut+' '+pProject+name
f.execute(command,name," with B0Checker")
f.execute(command.replace(" -c  "," -a "),name,"only adding the type information")

name = 'COMPREF_r.ref '
command = pBXML+' -c  -i4 -I '+ pProject + ' -O'+pOut+' '+pProject+name
f.execute(command,name," with B0Checker")
f.execute(command.replace(" -c  "," -a "),name,"only adding the type information")

name = 'COMPREF_i.imp '
command = pBXML+' -c  -i4 -I '+ pProject + ' -O'+pOut+' '+pProject+name
f.execute(command,name," with B0Checker")
f.execute(command.replace(" -c  "," -a "),name,"only adding the type information")

name = 'COMPIMP.mch '
command = pBXML+' -c  -i4 -I '+ pProject + ' -O'+pOut+' '+pProject+name
f.execute(command,name," with B0Checker")
f.execute(command.replace(" -c  "," -a "),name,"only adding the type information")

name = 'COMPIMP_INT.mch '
command = pBXML+' -c  -i4 -I '+ pProject + ' -O'+pOut+' '+pProject+name
f.execute(command,name," with B0Checker")
f.execute(command.replace(" -c  "," -a "),name,"only adding the type information")

name = 'COMPIMP_BOOL.mch '
command = pBXML+' -c  -i4 -I '+ pProject + ' -O'+pOut+' '+pProject+name
f.execute(command,name," with B0Checker")
f.execute(command.replace(" -c  "," -a "),name,"only adding the type information")

for i in f.frequency_status_code:
    print ( "The frequency of option and return code  ("+i+") is "+str(f.frequency_status_code.get(i)))

print("Simple tests:"+str(len(setOfTest))+" and aux components:"+str(6) )


print("*****************************************************************")
print("* 5.0 Calling the C4B                                           *")
print("*****************************************************************")
#pC4B ='./b2c '
pC4B ="/Applications/AtelierB_4.3/AtelierB.app/AB/bbin/macosx/b2c"
imps =[]
f.filename.count_name = f.execute.oks =0
for test in setOfTest:
    n = f.filename()
    command = pC4B+' -I '+ pProject + ' -C ./out_C '+pProject+n+'.imp '
    fi = os.popen(command)
    res = fi.read()
    fi.close()
    if "error" in res :
        print(n)
        print ("Error : "+test+"\n"+res)
    else:
        f.execute.oks+=1
        imps.append(n)
        print("OK - "+n)


compref_OK = f.fun_comp_extras(imps, pProject)

print("Tests b2c generated: "+str(f.execute.oks+compref_OK) + " and total tests:"+str(len(setOfTest)+compref_OK) )


print("*****************************************************************")
print("* 6.0 Calling gcc                                               *")
print("*****************************************************************")
f.filename.count_name = f.execute.oks = errors = 0
for name in imps:
    print(name)
    command = "clang -c -o ./out_C/"+name+".o ./out_C/"+name+".c"
    errors += f.executeSub(command)
print("Erros calling gcc:"+str(errors)+" C4B compiled tests:"+str(len(imps))+" B0 check "+ str(len(setOfTest)))

print("*****************************************************************")
print("* 7.0 Calling B2LLVM                                            *")
print("*****************************************************************")

f.creating_B_project(len(setOfTest))
approved_tests = f.calling_b2llvm()

print("*****************************************************************")
print("* 8.0 Calling the llc-mp                                        *")
print("*****************************************************************")
filenames_ok = []
filenames_bug = []
for filename in approved_tests:
    f.print_example_title(filename)
    cmd = "/opt/local/bin/llc-mp-3.5 "+ filename+ ".llvm -o "+ filename+".llvm.o"
    #print(cmd)
    res = f.executeSub(cmd,filename)
    if res == 0 :
        filenames_ok.append(filename)
    else:
        filenames_bug.append(filename)


print("Total passed in llc-mp:"+str(len(filenames_ok))+" and its names")
for i in filenames_ok:
    print(i)
print("Total with bug in llc-mp:"+str(len(filenames_bug))+" and its names")
for  i in filenames_bug:
    print(i)

if True:
    print("*****************************************************************")
    print("* Analyzing the coverage and input and output                   *")
    print("*****************************************************************")
    print("* Generating the coverage code report")
    print("Combining coverage data.")
    os.system("/usr/local/bin/coverage combine")
    print("Generating coverage report.")
    os.system("/usr/local/bin/coverage html -d coverage_html")
    os.system("open coverage_html/index.html")
    print("Coverage report is now available.")

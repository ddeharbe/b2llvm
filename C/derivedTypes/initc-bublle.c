#include "bubble_.h"
#include <stdio.h>

bubble$state$ bubble;

int main(){
	int i;
    printf("Original\n");
    for(int i=0;i<100;i++){
        bubble.vec1[i]=100-i;
        printf(" vec1[%d]= %d\n",i, bubble.vec1[i]  );
    }

	bubble$init$(&bubble);
    printf("After init\n");
	for(int i=0;i<=5;i++)
        printf(" vec1[%d]= %d\n",i, bubble.vec1[i]  );
    printf(" ...\n");

	bubble$op_sort(&bubble);
    printf("After sort\n");
    for(int i=0;i<100;i++)
        printf(" vec1[%d]= %d\n",i, bubble.vec1[i]  );
    

} 


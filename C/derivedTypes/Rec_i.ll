; ModuleID = 'Rec_i.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.11.0"

%struct.R_1 = type { i32, i32 }

@Rec__account = internal global %struct.R_1 zeroinitializer, align 4

; Function Attrs: nounwind ssp uwtable
define void @Rec__INITIALISATION() #0 {
  store i32 0, i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 0), align 4
  store i32 10, i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 1), align 4
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @Rec__positive(i8* %res) #0 {
  %1 = alloca i8*, align 8
  store i8* %res, i8** %1, align 8
  %2 = load i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 1), align 4
  %3 = icmp sgt i32 %2, 0
  br i1 %3, label %4, label %6

; <label>:4                                       ; preds = %0
  %5 = load i8** %1, align 8
  store i8 1, i8* %5, align 1
  br label %8

; <label>:6                                       ; preds = %0
  %7 = load i8** %1, align 8
  store i8 0, i8* %7, align 1
  br label %8

; <label>:8                                       ; preds = %6, %4
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @Rec__withrdaw(i32 %amt) #0 {
  %1 = alloca i32, align 4
  store i32 %amt, i32* %1, align 4
  %2 = load i32* %1, align 4
  %3 = icmp sge i32 %2, 0
  br i1 %3, label %4, label %16

; <label>:4                                       ; preds = %0
  %5 = load i32* %1, align 4
  %6 = icmp sle i32 %5, 2147483647
  br i1 %6, label %7, label %16

; <label>:7                                       ; preds = %4
  %8 = load i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 1), align 4
  %9 = load i32* %1, align 4
  %10 = icmp sge i32 %8, %9
  br i1 %10, label %11, label %16

; <label>:11                                      ; preds = %7
  %12 = load i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 0), align 4
  store i32 %12, i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 0), align 4
  %13 = load i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 1), align 4
  %14 = load i32* %1, align 4
  %15 = sub nsw i32 %13, %14
  store i32 %15, i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 1), align 4
  br label %16

; <label>:16                                      ; preds = %11, %7, %4, %0
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @Rec__unsafe_dec() #0 {
  %1 = load i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 1), align 4
  %2 = sub nsw i32 %1, 1
  store i32 %2, i32* getelementptr inbounds (%struct.R_1* @Rec__account, i32 0, i32 1), align 4
  ret void
}

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"Apple LLVM version 7.0.0 (clang-700.0.72)"}

; ModuleID = 'Record_Simple.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

%struct.database = type { i32, i32, float }

@.str = private unnamed_addr constant [44 x i8] c"employee.age:%d , id_number:%d, salary:%f \0A\00", align 1
@.str1 = private unnamed_addr constant [47 x i8] c"newemployee.age:%d , id_number:%d, salary:%f \0A\00", align 1
@n = common global [10 x i32] zeroinitializer, align 16
@nn = common global [10 x i32] zeroinitializer, align 16
@.str2 = private unnamed_addr constant [21 x i8] c"Element n:[%d] = %d\0A\00", align 1
@.str3 = private unnamed_addr constant [22 x i8] c"Element nn:[%d] = %d\0A\00", align 1

; Function Attrs: nounwind ssp uwtable
define void @test0() #0 {
  %employee = alloca %struct.database, align 4
  %newEmployee = alloca %struct.database, align 4
  %1 = getelementptr inbounds %struct.database* %employee, i32 0, i32 1
  store i32 22, i32* %1, align 4
  %2 = getelementptr inbounds %struct.database* %employee, i32 0, i32 0
  store i32 1, i32* %2, align 4
  %3 = getelementptr inbounds %struct.database* %employee, i32 0, i32 2
  store float 0x40C7701AE0000000, float* %3, align 4
  %4 = bitcast %struct.database* %newEmployee to i8*
  %5 = bitcast %struct.database* %employee to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %4, i8* %5, i64 12, i32 4, i1 false)
  %6 = getelementptr inbounds %struct.database* %newEmployee, i32 0, i32 1
  store i32 5, i32* %6, align 4
  %7 = getelementptr inbounds %struct.database* %employee, i32 0, i32 1
  %8 = load i32* %7, align 4
  %9 = getelementptr inbounds %struct.database* %employee, i32 0, i32 0
  %10 = load i32* %9, align 4
  %11 = getelementptr inbounds %struct.database* %employee, i32 0, i32 2
  %12 = load float* %11, align 4
  %13 = fpext float %12 to double
  %14 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([44 x i8]* @.str, i32 0, i32 0), i32 %8, i32 %10, double %13)
  %15 = getelementptr inbounds %struct.database* %newEmployee, i32 0, i32 1
  %16 = load i32* %15, align 4
  %17 = getelementptr inbounds %struct.database* %newEmployee, i32 0, i32 0
  %18 = load i32* %17, align 4
  %19 = getelementptr inbounds %struct.database* %newEmployee, i32 0, i32 2
  %20 = load float* %19, align 4
  %21 = fpext float %20 to double
  %22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([47 x i8]* @.str1, i32 0, i32 0), i32 %16, i32 %18, double %21)
  ret void
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1

declare i32 @printf(i8*, ...) #2

; Function Attrs: nounwind ssp uwtable
define void @test1() #0 {
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %1

; <label>:1                                       ; preds = %15, %0
  %2 = load i32* %i, align 4
  %3 = icmp slt i32 %2, 10
  br i1 %3, label %4, label %18

; <label>:4                                       ; preds = %1
  %5 = load i32* %i, align 4
  %6 = add nsw i32 %5, 100
  %7 = load i32* %i, align 4
  %8 = sext i32 %7 to i64
  %9 = getelementptr inbounds [10 x i32]* @n, i32 0, i64 %8
  store i32 %6, i32* %9, align 4
  %10 = load i32* %i, align 4
  %11 = add nsw i32 2, %10
  %12 = load i32* %i, align 4
  %13 = sext i32 %12 to i64
  %14 = getelementptr inbounds [10 x i32]* @nn, i32 0, i64 %13
  store i32 %11, i32* %14, align 4
  br label %15

; <label>:15                                      ; preds = %4
  %16 = load i32* %i, align 4
  %17 = add nsw i32 %16, 1
  store i32 %17, i32* %i, align 4
  br label %1

; <label>:18                                      ; preds = %1
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @test2() #0 {
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i32 0, i32* %j, align 4
  br label %1

; <label>:1                                       ; preds = %17, %0
  %2 = load i32* %j, align 4
  %3 = icmp slt i32 %2, 10
  br i1 %3, label %4, label %20

; <label>:4                                       ; preds = %1
  %5 = load i32* %j, align 4
  %6 = load i32* %j, align 4
  %7 = sext i32 %6 to i64
  %8 = getelementptr inbounds [10 x i32]* @n, i32 0, i64 %7
  %9 = load i32* %8, align 4
  %10 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([21 x i8]* @.str2, i32 0, i32 0), i32 %5, i32 %9)
  %11 = load i32* %j, align 4
  %12 = load i32* %j, align 4
  %13 = sext i32 %12 to i64
  %14 = getelementptr inbounds [10 x i32]* @nn, i32 0, i64 %13
  %15 = load i32* %14, align 4
  %16 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([22 x i8]* @.str3, i32 0, i32 0), i32 %11, i32 %15)
  br label %17

; <label>:17                                      ; preds = %4
  %18 = load i32* %j, align 4
  %19 = add nsw i32 %18, 1
  store i32 %19, i32* %j, align 4
  br label %1

; <label>:20                                      ; preds = %1
  ret void
}

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  call void @test0()
  call void @test1()
  call void @test2()
  ret i32 0
}

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.49) (based on LLVM 3.6.0svn)"}

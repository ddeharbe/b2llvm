; ModuleID = 'declareArray_init.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

@main.larray = private unnamed_addr constant [3 x i32] [i32 10, i32 10, i32 10], align 4
@main.larray3 = private unnamed_addr constant [3 x [3 x i32]] [[3 x i32] [i32 0, i32 1, i32 2], [3 x i32] [i32 10, i32 11, i32 12], [3 x i32] [i32 20, i32 21, i32 22]], align 16
@main.larray4 = private unnamed_addr constant [3 x [3 x i32]] [[3 x i32] [i32 1, i32 5, i32 5], [3 x i32] [i32 2, i32 2, i32 2], [3 x i32] [i32 3, i32 3, i32 3]], align 16
@main.larray5 = private unnamed_addr constant [3 x [3 x [3 x i32]]] [[3 x [3 x i32]] [[3 x i32] [i32 1, i32 5, i32 5], [3 x i32] [i32 2, i32 2, i32 2], [3 x i32] [i32 3, i32 3, i32 3]], [3 x [3 x i32]] [[3 x i32] [i32 1, i32 5, i32 5], [3 x i32] [i32 2, i32 2, i32 2], [3 x i32] [i32 3, i32 3, i32 3]], [3 x [3 x i32]] [[3 x i32] [i32 1, i32 5, i32 5], [3 x i32] [i32 2, i32 2, i32 2], [3 x i32] [i32 3, i32 3, i32 3]]], align 16

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  %larray = alloca [3 x i32], align 4
  %larray3 = alloca [3 x [3 x i32]], align 16
  %larray4 = alloca [3 x [3 x i32]], align 16
  %larray5 = alloca [3 x [3 x [3 x i32]]], align 16
  %1 = bitcast [3 x i32]* %larray to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1, i8* bitcast ([3 x i32]* @main.larray to i8*), i64 12, i32 4, i1 false)
  %2 = bitcast [3 x [3 x i32]]* %larray3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %2, i8* bitcast ([3 x [3 x i32]]* @main.larray3 to i8*), i64 36, i32 16, i1 false)
  %3 = bitcast [3 x [3 x i32]]* %larray4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %3, i8* bitcast ([3 x [3 x i32]]* @main.larray4 to i8*), i64 36, i32 16, i1 false)
  %4 = bitcast [3 x [3 x [3 x i32]]]* %larray5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %4, i8* bitcast ([3 x [3 x [3 x i32]]]* @main.larray5 to i8*), i64 108, i32 16, i1 false)
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.49) (based on LLVM 3.6.0svn)"}

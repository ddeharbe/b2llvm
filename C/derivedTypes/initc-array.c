#include "Array.h"
#include <stdio.h>
const bool debug = false;
array$state$ array;

int main(){
	int i,j;
    // Manual reset
    for(int i=0;i<100;i++){
        for(int j=0;j<100;j++){
        	array.arr2d[i][j]=2;
        }
    }
   // Manual reset
    for(int i=0;i<100;i++){
        array.arr[i]=15;
        array.tmp[i]=2;
    }

    // Call init
    array$init$(&array);
    
    if (debug){
    for(int i=0;i<11;i++){
        for(int j=0;j<11;j++){
            printf(" [%d][%d]=%d\n",i, j, array.arr2d[i][j]);
        }
        printf("\n");
    }
    }

    if(array.arr[3]==10)
        printf("OK - arr\n");
    else
        printf("%d\nError line:%d\n",array.arr[3],__LINE__);
    
    if(array.tint1==7 && array.tint2==777 )
        printf("OK - tint\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    if(array.arr2d[0][0]==9)
        printf("OK - arr2d\n");
    else
        printf("arr2d[0][0] = 9 != %d\nError line:%d\n",array.arr2d[0][0],__LINE__);
    
    if(array.arr2d[1][1]==99)
        printf("OK - arr2d\n");
    else
        printf("arr2d[1][1] =99 != %d\nError line:%d\n",array.arr2d[1][1],__LINE__);
    
    
    if(array.tmp[1]==10)
        printf("OK - arr\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    if(array.arr[1]==33)
        printf("OK - arr\n");
    else
        printf("Error line:%d\n",__LINE__);

    bool error_copy_array=false;
    
    for(int i=0;i<100;i++){
        if( array.arr[i]!= 15 && !( i==1 && array.arr[i]==33) && !( i==3 && array.arr[i]==10) ){
            printf("Error line:%d  arr:%d->%d \n",__LINE__,i,array.arr[i]);
            error_copy_array=true;
        }
    }

    for(int i=0;i<100;i++){
        if( array.tmp[i]!= 15 && !( i==1 && array.tmp[i]==10) && !( i==3 && array.tmp[i]==10) ){
            printf("Error line:%d  arr:%d->%d \n",__LINE__,i,array.tmp[i]);
                        error_copy_array=true;
        }
    }
    if(!error_copy_array)
        printf("OK - copy tmp := arr\n");
    
    
    if(array.a3d[1][1][3]==9 && array.a3d[0][0][1]==100 && array.a3d[0][1][1]==100)
        printf("OK - a3d\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    if(array.a3dcopy[1][0][0]==77 && array.a3d[1][0][0]!=77)
        printf("OK - a3d copy array\n");
    else
        printf("Error line:%d\n",__LINE__);
    
    bool error_init_array_CartProduct =false;
    for(int i=0;i<=3;i++){
        for(int j=0;j<=3;j++){
            if (array.arr2CProduct[i][j]!=4){
                error_init_array_CartProduct=true;
                printf("Error line:%d\n",__LINE__);
            }
        }
    }
    if(!error_init_array_CartProduct)
    printf("OK - init array with Cartesian Product ( 2 Dimensions) \n");
    
    error_init_array_CartProduct =false;
    for(int i=0;i<=2;i++){
        for(int j=0;j<=3;j++){
            for(int k=0;k<=4;k++){
                if (array.arr3Cproduct[i][j][k]!=10){
                    error_init_array_CartProduct=true;
                    printf("%d,%d,%d -> %d ", i,j,k,array.arr3Cproduct[i][j][k]);
                    printf("Error line:%d\n",__LINE__);
                }
            }
        }
    }
    if(!error_init_array_CartProduct)
        printf("OK - init array with Cartesian Product ( 3 Dimensions) of integer \n");
    
    
/* Case to verify
    error_init_array_CartProduct =false;
    for(int i=0;i<=1;i++){
        for(int j=0;j<=5;j++){
            for(int k=0;k<=3;k++){
                if (array.arr4Cprod_bool[i][j][k]!=0){
                    error_init_array_CartProduct=true;
                    printf("%d,%d,%d -> %d ", i,j,k,array.arr4Cprod_bool[i][j][k]);
                    printf("Error line:%d\n",__LINE__);
                }
            }
        }
    }
    if(!error_init_array_CartProduct)
        printf("OK - init array with Cartesian Product ( 3 Dimensions) of bool \n");
*/
    

}


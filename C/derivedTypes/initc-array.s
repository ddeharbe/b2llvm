	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 10
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$72, %rsp
Ltmp3:
	.cfi_offset %rbx, -40
Ltmp4:
	.cfi_offset %r14, -32
Ltmp5:
	.cfi_offset %r15, -24
	movl	$0, -28(%rbp)
	movl	$0, -40(%rbp)
	movq	_array@GOTPCREL(%rip), %r14
	jmp	LBB0_1
	.align	4, 0x90
LBB0_5:                                 ##   in Loop: Header=BB0_1 Depth=1
	incl	-40(%rbp)
LBB0_1:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB0_3 Depth 2
	cmpl	$99, -40(%rbp)
	jg	LBB0_6
## BB#2:                                ##   in Loop: Header=BB0_1 Depth=1
	movl	$0, -44(%rbp)
	jmp	LBB0_3
	.align	4, 0x90
LBB0_4:                                 ##   in Loop: Header=BB0_3 Depth=2
	movslq	-44(%rbp), %rax
	movslq	-40(%rbp), %rcx
	imulq	 $400, %rcx             ## imm = 0x190
	addq	%r14, %rcx
	movl	$2, 400(%rcx,%rax,4)
	incl	-44(%rbp)
LBB0_3:                                 ##   Parent Loop BB0_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpl	$99, -44(%rbp)
	jle	LBB0_4
	jmp	LBB0_5
LBB0_6:
	movl	$0, -48(%rbp)
	jmp	LBB0_7
	.align	4, 0x90
LBB0_8:                                 ##   in Loop: Header=BB0_7 Depth=1
	movslq	-48(%rbp), %rax
	movl	$15, (%r14,%rax,4)
	movslq	-48(%rbp), %rax
	movl	$2, 40400(%r14,%rax,4)
	incl	-48(%rbp)
LBB0_7:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$99, -48(%rbp)
	jle	LBB0_8
## BB#9:
	movq	%r14, %rdi
	callq	_array$init$
	cmpl	$10, 12(%r14)
	jne	LBB0_11
## BB#10:
	leaq	L_.str(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_12
LBB0_11:
	movl	12(%r14), %esi
	leaq	L_.str1(%rip), %rdi
	movl	$35, %edx
	xorl	%eax, %eax
	callq	_printf
LBB0_12:
	cmpl	$7, 40928(%r14)
	jne	LBB0_15
## BB#13:
	cmpl	$777, 40932(%r14)       ## imm = 0x309
	jne	LBB0_15
## BB#14:
	leaq	L_.str2(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_16
LBB0_15:
	leaq	L_.str3(%rip), %rdi
	movl	$40, %esi
	xorl	%eax, %eax
	callq	_printf
LBB0_16:
	cmpl	$9, 400(%r14)
	jne	LBB0_18
## BB#17:
	leaq	L_.str4(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_19
LBB0_18:
	movl	400(%r14), %esi
	leaq	L_.str5(%rip), %rdi
	movl	$45, %edx
	xorl	%eax, %eax
	callq	_printf
LBB0_19:
	cmpl	$99, 804(%r14)
	jne	LBB0_21
## BB#20:
	leaq	L_.str4(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_22
LBB0_21:
	movl	804(%r14), %esi
	leaq	L_.str6(%rip), %rdi
	movl	$50, %edx
	xorl	%eax, %eax
	callq	_printf
LBB0_22:
	cmpl	$10, 40404(%r14)
	jne	LBB0_24
## BB#23:
	leaq	L_.str(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_25
LBB0_24:
	leaq	L_.str3(%rip), %rdi
	movl	$56, %esi
	xorl	%eax, %eax
	callq	_printf
LBB0_25:
	cmpl	$33, 4(%r14)
	jne	LBB0_27
## BB#26:
	leaq	L_.str(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_28
LBB0_27:
	leaq	L_.str3(%rip), %rdi
	movl	$61, %esi
	xorl	%eax, %eax
	callq	_printf
LBB0_28:
	movb	$0, -49(%rbp)
	movl	$0, -56(%rbp)
	leaq	L_.str7(%rip), %rbx
	jmp	LBB0_29
	.align	4, 0x90
LBB0_36:                                ##   in Loop: Header=BB0_29 Depth=1
	incl	-56(%rbp)
LBB0_29:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$99, -56(%rbp)
	jg	LBB0_37
## BB#30:                               ##   in Loop: Header=BB0_29 Depth=1
	movslq	-56(%rbp), %rax
	cmpl	$15, (%r14,%rax,4)
	je	LBB0_36
## BB#31:                               ##   in Loop: Header=BB0_29 Depth=1
	cmpl	$1, -56(%rbp)
	jne	LBB0_33
## BB#32:                               ##   in Loop: Header=BB0_29 Depth=1
	movslq	-56(%rbp), %rax
	cmpl	$33, (%r14,%rax,4)
	je	LBB0_36
LBB0_33:                                ##   in Loop: Header=BB0_29 Depth=1
	cmpl	$3, -56(%rbp)
	jne	LBB0_35
## BB#34:                               ##   in Loop: Header=BB0_29 Depth=1
	movslq	-56(%rbp), %rax
	cmpl	$10, (%r14,%rax,4)
	je	LBB0_36
LBB0_35:                                ##   in Loop: Header=BB0_29 Depth=1
	movslq	-56(%rbp), %rdx
	movl	(%r14,%rdx,4), %ecx
	movl	$67, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
                                        ## kill: EDX<def> EDX<kill> RDX<kill>
	callq	_printf
	movb	$1, -49(%rbp)
	jmp	LBB0_36
LBB0_37:
	movl	$0, -60(%rbp)
	leaq	L_.str7(%rip), %rbx
	jmp	LBB0_38
	.align	4, 0x90
LBB0_45:                                ##   in Loop: Header=BB0_38 Depth=1
	incl	-60(%rbp)
LBB0_38:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$99, -60(%rbp)
	jg	LBB0_46
## BB#39:                               ##   in Loop: Header=BB0_38 Depth=1
	movslq	-60(%rbp), %rax
	cmpl	$15, 40400(%r14,%rax,4)
	je	LBB0_45
## BB#40:                               ##   in Loop: Header=BB0_38 Depth=1
	cmpl	$1, -60(%rbp)
	jne	LBB0_42
## BB#41:                               ##   in Loop: Header=BB0_38 Depth=1
	movslq	-60(%rbp), %rax
	cmpl	$10, 40400(%r14,%rax,4)
	je	LBB0_45
LBB0_42:                                ##   in Loop: Header=BB0_38 Depth=1
	cmpl	$3, -60(%rbp)
	jne	LBB0_44
## BB#43:                               ##   in Loop: Header=BB0_38 Depth=1
	movslq	-60(%rbp), %rax
	cmpl	$10, 40400(%r14,%rax,4)
	je	LBB0_45
LBB0_44:                                ##   in Loop: Header=BB0_38 Depth=1
	movslq	-60(%rbp), %rdx
	movl	40400(%r14,%rdx,4), %ecx
	movl	$74, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
                                        ## kill: EDX<def> EDX<kill> RDX<kill>
	callq	_printf
	movb	$1, -49(%rbp)
	jmp	LBB0_45
LBB0_46:
	testb	$1, -49(%rbp)
	jne	LBB0_48
## BB#47:
	leaq	L_.str8(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
LBB0_48:
	cmpl	$9, 40860(%r14)
	jne	LBB0_52
## BB#49:
	cmpl	$100, 40804(%r14)
	jne	LBB0_52
## BB#50:
	cmpl	$100, 40820(%r14)
	jne	LBB0_52
## BB#51:
	leaq	L_.str9(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_53
LBB0_52:
	leaq	L_.str3(%rip), %rdi
	movl	$85, %esi
	xorl	%eax, %eax
	callq	_printf
LBB0_53:
	cmpl	$77, 40896(%r14)
	jne	LBB0_56
## BB#54:
	cmpl	$77, 40832(%r14)
	je	LBB0_56
## BB#55:
	leaq	L_.str10(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
	jmp	LBB0_57
LBB0_56:
	leaq	L_.str3(%rip), %rdi
	movl	$90, %esi
	xorl	%eax, %eax
	callq	_printf
LBB0_57:
	movb	$0, -61(%rbp)
	movl	$0, -68(%rbp)
	leaq	L_.str3(%rip), %rbx
	jmp	LBB0_58
	.align	4, 0x90
LBB0_64:                                ##   in Loop: Header=BB0_58 Depth=1
	incl	-68(%rbp)
LBB0_58:                                ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB0_60 Depth 2
	cmpl	$3, -68(%rbp)
	jg	LBB0_65
## BB#59:                               ##   in Loop: Header=BB0_58 Depth=1
	movl	$0, -72(%rbp)
	jmp	LBB0_60
	.align	4, 0x90
LBB0_63:                                ##   in Loop: Header=BB0_60 Depth=2
	incl	-72(%rbp)
LBB0_60:                                ##   Parent Loop BB0_58 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpl	$3, -72(%rbp)
	jg	LBB0_64
## BB#61:                               ##   in Loop: Header=BB0_60 Depth=2
	movslq	-72(%rbp), %rax
	movslq	-68(%rbp), %rcx
	shlq	$4, %rcx
	addq	%r14, %rcx
	cmpl	$4, 41336(%rcx,%rax,4)
	je	LBB0_63
## BB#62:                               ##   in Loop: Header=BB0_60 Depth=2
	movb	$1, -61(%rbp)
	movl	$97, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	_printf
	jmp	LBB0_63
LBB0_65:
	testb	$1, -61(%rbp)
	jne	LBB0_67
## BB#66:
	leaq	L_.str11(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
LBB0_67:
	movb	$0, -61(%rbp)
	movl	$0, -76(%rbp)
	leaq	L_.str12(%rip), %r15
	leaq	L_.str3(%rip), %rbx
	jmp	LBB0_68
	.align	4, 0x90
LBB0_77:                                ##   in Loop: Header=BB0_68 Depth=1
	incl	-76(%rbp)
LBB0_68:                                ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB0_70 Depth 2
                                        ##       Child Loop BB0_72 Depth 3
	cmpl	$2, -76(%rbp)
	jg	LBB0_78
## BB#69:                               ##   in Loop: Header=BB0_68 Depth=1
	movl	$0, -80(%rbp)
	jmp	LBB0_70
	.align	4, 0x90
LBB0_76:                                ##   in Loop: Header=BB0_70 Depth=2
	incl	-80(%rbp)
LBB0_70:                                ##   Parent Loop BB0_68 Depth=1
                                        ## =>  This Loop Header: Depth=2
                                        ##       Child Loop BB0_72 Depth 3
	cmpl	$3, -80(%rbp)
	jg	LBB0_77
## BB#71:                               ##   in Loop: Header=BB0_70 Depth=2
	movl	$0, -84(%rbp)
	jmp	LBB0_72
	.align	4, 0x90
LBB0_75:                                ##   in Loop: Header=BB0_72 Depth=3
	incl	-84(%rbp)
LBB0_72:                                ##   Parent Loop BB0_68 Depth=1
                                        ##     Parent Loop BB0_70 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	cmpl	$4, -84(%rbp)
	jg	LBB0_76
## BB#73:                               ##   in Loop: Header=BB0_72 Depth=3
	movslq	-84(%rbp), %rax
	movslq	-80(%rbp), %rcx
	movslq	-76(%rbp), %rdx
	imulq	 $2604, %rdx            ## imm = 0xA2C
	addq	%r14, %rdx
	imulq	 $124, %rcx
	addq	%rdx, %rcx
	cmpl	$10, 41400(%rcx,%rax,4)
	je	LBB0_75
## BB#74:                               ##   in Loop: Header=BB0_72 Depth=3
	movb	$1, -61(%rbp)
	movslq	-76(%rbp), %rsi
	movslq	-80(%rbp), %rdx
	movslq	-84(%rbp), %rcx
	imulq	$2604, %rsi, %rax       ## imm = 0xA2C
	addq	%r14, %rax
	imulq	$124, %rdx, %rdi
	addq	%rax, %rdi
	movl	41400(%rdi,%rcx,4), %r8d
	xorl	%eax, %eax
	movq	%r15, %rdi
                                        ## kill: ESI<def> ESI<kill> RSI<kill>
                                        ## kill: EDX<def> EDX<kill> RDX<kill>
                                        ## kill: ECX<def> ECX<kill> RCX<kill>
	callq	_printf
	movl	$111, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	_printf
	jmp	LBB0_75
LBB0_78:
	testb	$1, -61(%rbp)
	jne	LBB0_80
## BB#79:
	leaq	L_.str13(%rip), %rdi
	xorl	%eax, %eax
	callq	_printf
LBB0_80:
	movl	-28(%rbp), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__const
	.globl	_debug                  ## @debug
_debug:
	.byte	0                       ## 0x0

	.comm	_array,70236,2          ## @array
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"OK - arr\n"

L_.str1:                                ## @.str1
	.asciz	"%d\nError line:%d\n"

L_.str2:                                ## @.str2
	.asciz	"OK - tint\n"

L_.str3:                                ## @.str3
	.asciz	"Error line:%d\n"

L_.str4:                                ## @.str4
	.asciz	"OK - arr2d\n"

L_.str5:                                ## @.str5
	.asciz	"arr2d[0][0] = 9 != %d\nError line:%d\n"

L_.str6:                                ## @.str6
	.asciz	"arr2d[1][1] =99 != %d\nError line:%d\n"

L_.str7:                                ## @.str7
	.asciz	"Error line:%d  arr:%d->%d \n"

L_.str8:                                ## @.str8
	.asciz	"OK - copy tmp := arr\n"

L_.str9:                                ## @.str9
	.asciz	"OK - a3d\n"

L_.str10:                               ## @.str10
	.asciz	"OK - a3d copy array\n"

L_.str11:                               ## @.str11
	.asciz	"OK - init array with Cartesian Product ( 2 Dimensions) \n"

L_.str12:                               ## @.str12
	.asciz	"%d,%d,%d -> %d "

L_.str13:                               ## @.str13
	.asciz	"OK - init array with Cartesian Product ( 3 Dimensions) of integer \n"


.subsections_via_symbols

#ifndef _array_h
#define _array_h

#include <stdint.h>
#include <stdbool.h>

typedef struct {int32_t arr[100]; int32_t arr2d[100][100]; int32_t tmp[100]; int32_t a3d[2][2][4]; int32_t a3dcopy[2][2][4]; int32_t tint1; int32_t tint2; int32_t arrinit[100]; int32_t arr2CProduct[4][4]; int32_t arr3Cproduct[11][21][31]; int32_t arr4Cprod_bool[2][6][4]; }   array$state$;
typedef array$state$ * array$ref$;
extern void array$init$(array$ref$ self);
#endif /* _array_h */

; ModuleID = 'const.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

@main.larray2 = private unnamed_addr constant [4 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]], align 16

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  %larray2 = alloca [4 x [4 x i32]], align 16
  %1 = bitcast [4 x [4 x i32]]* %larray2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1, i8* bitcast ([4 x [4 x i32]]* @main.larray2 to i8*), i64 64, i32 16, i1 false)
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.49) (based on LLVM 3.6.0svn)"}

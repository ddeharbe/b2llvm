; ModuleID = 'array_example.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

%struct.tstruct = type { [99 x i32], [2 x [2 x [4 x i32]]], i32, i32 }

@start_arr = constant i32 1, align 4
@array = global [3 x [3 x [4 x i32]]] [[3 x [4 x i32]] [[4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5]], [3 x [4 x i32]] [[4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5]], [3 x [4 x i32]] [[4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5]]], align 16
@.str = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@ref = common global %struct.tstruct zeroinitializer, align 4

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  call void @set()
  %1 = load i32* getelementptr inbounds (%struct.tstruct* @ref, i32 0, i32 0, i64 4), align 4
  %2 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @.str, i32 0, i32 0), i32 %1)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

; Function Attrs: nounwind ssp uwtable
define void @set() #0 {
  %ind = alloca i32, align 4
  %tmp = alloca i32, align 4
  store i32 7, i32* %ind, align 4
  store i32 3, i32* %tmp, align 4
  %1 = load i32* %tmp, align 4
  %2 = sext i32 %1 to i64
  %3 = getelementptr inbounds [99 x i32]* getelementptr inbounds (%struct.tstruct* @ref, i32 0, i32 0), i32 0, i64 %2
  store i32 5, i32* %3, align 4
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @get() #0 {
  %a = alloca i32, align 4
  %1 = load i32* getelementptr inbounds (%struct.tstruct* @ref, i32 0, i32 0, i64 4), align 4
  store i32 %1, i32* %a, align 4
  ret void
}

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.49) (based on LLVM 3.6.0svn)"}

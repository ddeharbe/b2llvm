; ModuleID = 'initc-array.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

%"struct.array$state$" = type { [100 x i32], [100 x [100 x i32]], [100 x i32], [2 x [2 x [4 x i32]]], [2 x [2 x [4 x i32]]], i32, i32, [100 x i32], [4 x [4 x i32]], [11 x [21 x [31 x i32]]], [2 x [6 x [4 x i32]]] }

@debug = constant i8 0, align 1
@array = common global %"struct.array$state$" zeroinitializer, align 4
@.str = private unnamed_addr constant [10 x i8] c"OK - arr\0A\00", align 1
@.str1 = private unnamed_addr constant [18 x i8] c"%d\0AError line:%d\0A\00", align 1
@.str2 = private unnamed_addr constant [11 x i8] c"OK - tint\0A\00", align 1
@.str3 = private unnamed_addr constant [15 x i8] c"Error line:%d\0A\00", align 1
@.str4 = private unnamed_addr constant [12 x i8] c"OK - arr2d\0A\00", align 1
@.str5 = private unnamed_addr constant [37 x i8] c"arr2d[0][0] = 9 != %d\0AError line:%d\0A\00", align 1
@.str6 = private unnamed_addr constant [37 x i8] c"arr2d[1][1] =99 != %d\0AError line:%d\0A\00", align 1
@.str7 = private unnamed_addr constant [28 x i8] c"Error line:%d  arr:%d->%d \0A\00", align 1
@.str8 = private unnamed_addr constant [22 x i8] c"OK - copy tmp := arr\0A\00", align 1
@.str9 = private unnamed_addr constant [10 x i8] c"OK - a3d\0A\00", align 1
@.str10 = private unnamed_addr constant [21 x i8] c"OK - a3d copy array\0A\00", align 1
@.str11 = private unnamed_addr constant [57 x i8] c"OK - init array with Cartesian Product ( 2 Dimensions) \0A\00", align 1
@.str12 = private unnamed_addr constant [16 x i8] c"%d,%d,%d -> %d \00", align 1
@.str13 = private unnamed_addr constant [68 x i8] c"OK - init array with Cartesian Product ( 3 Dimensions) of integer \0A\00", align 1

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %i1 = alloca i32, align 4
  %j2 = alloca i32, align 4
  %i3 = alloca i32, align 4
  %error_copy_array = alloca i8, align 1
  %i4 = alloca i32, align 4
  %i5 = alloca i32, align 4
  %error_init_array_CartProduct = alloca i8, align 1
  %i6 = alloca i32, align 4
  %j7 = alloca i32, align 4
  %i8 = alloca i32, align 4
  %j9 = alloca i32, align 4
  %k = alloca i32, align 4
  store i32 0, i32* %1
  store i32 0, i32* %i1, align 4
  br label %2

; <label>:2                                       ; preds = %20, %0
  %3 = load i32* %i1, align 4
  %4 = icmp slt i32 %3, 100
  br i1 %4, label %5, label %23

; <label>:5                                       ; preds = %2
  store i32 0, i32* %j2, align 4
  br label %6

; <label>:6                                       ; preds = %16, %5
  %7 = load i32* %j2, align 4
  %8 = icmp slt i32 %7, 100
  br i1 %8, label %9, label %19

; <label>:9                                       ; preds = %6
  %10 = load i32* %j2, align 4
  %11 = sext i32 %10 to i64
  %12 = load i32* %i1, align 4
  %13 = sext i32 %12 to i64
  %14 = getelementptr inbounds [100 x [100 x i32]]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 1), i32 0, i64 %13
  %15 = getelementptr inbounds [100 x i32]* %14, i32 0, i64 %11
  store i32 2, i32* %15, align 4
  br label %16

; <label>:16                                      ; preds = %9
  %17 = load i32* %j2, align 4
  %18 = add nsw i32 %17, 1
  store i32 %18, i32* %j2, align 4
  br label %6

; <label>:19                                      ; preds = %6
  br label %20

; <label>:20                                      ; preds = %19
  %21 = load i32* %i1, align 4
  %22 = add nsw i32 %21, 1
  store i32 %22, i32* %i1, align 4
  br label %2

; <label>:23                                      ; preds = %2
  store i32 0, i32* %i3, align 4
  br label %24

; <label>:24                                      ; preds = %34, %23
  %25 = load i32* %i3, align 4
  %26 = icmp slt i32 %25, 100
  br i1 %26, label %27, label %37

; <label>:27                                      ; preds = %24
  %28 = load i32* %i3, align 4
  %29 = sext i32 %28 to i64
  %30 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0), i32 0, i64 %29
  store i32 15, i32* %30, align 4
  %31 = load i32* %i3, align 4
  %32 = sext i32 %31 to i64
  %33 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 2), i32 0, i64 %32
  store i32 2, i32* %33, align 4
  br label %34

; <label>:34                                      ; preds = %27
  %35 = load i32* %i3, align 4
  %36 = add nsw i32 %35, 1
  store i32 %36, i32* %i3, align 4
  br label %24

; <label>:37                                      ; preds = %24
  call void @"array$init$"(%"struct.array$state$"* @array)
  %38 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0, i64 3), align 4
  %39 = icmp eq i32 %38, 10
  br i1 %39, label %40, label %42

; <label>:40                                      ; preds = %37
  %41 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  br label %45

; <label>:42                                      ; preds = %37
  %43 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0, i64 3), align 4
  %44 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([18 x i8]* @.str1, i32 0, i32 0), i32 %43, i32 35)
  br label %45

; <label>:45                                      ; preds = %42, %40
  %46 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 5), align 4
  %47 = icmp eq i32 %46, 7
  br i1 %47, label %48, label %53

; <label>:48                                      ; preds = %45
  %49 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 6), align 4
  %50 = icmp eq i32 %49, 777
  br i1 %50, label %51, label %53

; <label>:51                                      ; preds = %48
  %52 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([11 x i8]* @.str2, i32 0, i32 0))
  br label %55

; <label>:53                                      ; preds = %48, %45
  %54 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 40)
  br label %55

; <label>:55                                      ; preds = %53, %51
  %56 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 1, i64 0, i64 0), align 4
  %57 = icmp eq i32 %56, 9
  br i1 %57, label %58, label %60

; <label>:58                                      ; preds = %55
  %59 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str4, i32 0, i32 0))
  br label %63

; <label>:60                                      ; preds = %55
  %61 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 1, i64 0, i64 0), align 4
  %62 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([37 x i8]* @.str5, i32 0, i32 0), i32 %61, i32 45)
  br label %63

; <label>:63                                      ; preds = %60, %58
  %64 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 1, i64 1, i64 1), align 4
  %65 = icmp eq i32 %64, 99
  br i1 %65, label %66, label %68

; <label>:66                                      ; preds = %63
  %67 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str4, i32 0, i32 0))
  br label %71

; <label>:68                                      ; preds = %63
  %69 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 1, i64 1, i64 1), align 4
  %70 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([37 x i8]* @.str6, i32 0, i32 0), i32 %69, i32 50)
  br label %71

; <label>:71                                      ; preds = %68, %66
  %72 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 2, i64 1), align 4
  %73 = icmp eq i32 %72, 10
  br i1 %73, label %74, label %76

; <label>:74                                      ; preds = %71
  %75 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  br label %78

; <label>:76                                      ; preds = %71
  %77 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 56)
  br label %78

; <label>:78                                      ; preds = %76, %74
  %79 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0, i64 1), align 4
  %80 = icmp eq i32 %79, 33
  br i1 %80, label %81, label %83

; <label>:81                                      ; preds = %78
  %82 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  br label %85

; <label>:83                                      ; preds = %78
  %84 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 61)
  br label %85

; <label>:85                                      ; preds = %83, %81
  store i8 0, i8* %error_copy_array, align 1
  store i32 0, i32* %i4, align 4
  br label %86

; <label>:86                                      ; preds = %121, %85
  %87 = load i32* %i4, align 4
  %88 = icmp slt i32 %87, 100
  br i1 %88, label %89, label %124

; <label>:89                                      ; preds = %86
  %90 = load i32* %i4, align 4
  %91 = sext i32 %90 to i64
  %92 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0), i32 0, i64 %91
  %93 = load i32* %92, align 4
  %94 = icmp ne i32 %93, 15
  br i1 %94, label %95, label %120

; <label>:95                                      ; preds = %89
  %96 = load i32* %i4, align 4
  %97 = icmp eq i32 %96, 1
  br i1 %97, label %98, label %104

; <label>:98                                      ; preds = %95
  %99 = load i32* %i4, align 4
  %100 = sext i32 %99 to i64
  %101 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0), i32 0, i64 %100
  %102 = load i32* %101, align 4
  %103 = icmp eq i32 %102, 33
  br i1 %103, label %120, label %104

; <label>:104                                     ; preds = %98, %95
  %105 = load i32* %i4, align 4
  %106 = icmp eq i32 %105, 3
  br i1 %106, label %107, label %113

; <label>:107                                     ; preds = %104
  %108 = load i32* %i4, align 4
  %109 = sext i32 %108 to i64
  %110 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0), i32 0, i64 %109
  %111 = load i32* %110, align 4
  %112 = icmp eq i32 %111, 10
  br i1 %112, label %120, label %113

; <label>:113                                     ; preds = %107, %104
  %114 = load i32* %i4, align 4
  %115 = load i32* %i4, align 4
  %116 = sext i32 %115 to i64
  %117 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 0), i32 0, i64 %116
  %118 = load i32* %117, align 4
  %119 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([28 x i8]* @.str7, i32 0, i32 0), i32 67, i32 %114, i32 %118)
  store i8 1, i8* %error_copy_array, align 1
  br label %120

; <label>:120                                     ; preds = %113, %107, %98, %89
  br label %121

; <label>:121                                     ; preds = %120
  %122 = load i32* %i4, align 4
  %123 = add nsw i32 %122, 1
  store i32 %123, i32* %i4, align 4
  br label %86

; <label>:124                                     ; preds = %86
  store i32 0, i32* %i5, align 4
  br label %125

; <label>:125                                     ; preds = %160, %124
  %126 = load i32* %i5, align 4
  %127 = icmp slt i32 %126, 100
  br i1 %127, label %128, label %163

; <label>:128                                     ; preds = %125
  %129 = load i32* %i5, align 4
  %130 = sext i32 %129 to i64
  %131 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 2), i32 0, i64 %130
  %132 = load i32* %131, align 4
  %133 = icmp ne i32 %132, 15
  br i1 %133, label %134, label %159

; <label>:134                                     ; preds = %128
  %135 = load i32* %i5, align 4
  %136 = icmp eq i32 %135, 1
  br i1 %136, label %137, label %143

; <label>:137                                     ; preds = %134
  %138 = load i32* %i5, align 4
  %139 = sext i32 %138 to i64
  %140 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 2), i32 0, i64 %139
  %141 = load i32* %140, align 4
  %142 = icmp eq i32 %141, 10
  br i1 %142, label %159, label %143

; <label>:143                                     ; preds = %137, %134
  %144 = load i32* %i5, align 4
  %145 = icmp eq i32 %144, 3
  br i1 %145, label %146, label %152

; <label>:146                                     ; preds = %143
  %147 = load i32* %i5, align 4
  %148 = sext i32 %147 to i64
  %149 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 2), i32 0, i64 %148
  %150 = load i32* %149, align 4
  %151 = icmp eq i32 %150, 10
  br i1 %151, label %159, label %152

; <label>:152                                     ; preds = %146, %143
  %153 = load i32* %i5, align 4
  %154 = load i32* %i5, align 4
  %155 = sext i32 %154 to i64
  %156 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 2), i32 0, i64 %155
  %157 = load i32* %156, align 4
  %158 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([28 x i8]* @.str7, i32 0, i32 0), i32 74, i32 %153, i32 %157)
  store i8 1, i8* %error_copy_array, align 1
  br label %159

; <label>:159                                     ; preds = %152, %146, %137, %128
  br label %160

; <label>:160                                     ; preds = %159
  %161 = load i32* %i5, align 4
  %162 = add nsw i32 %161, 1
  store i32 %162, i32* %i5, align 4
  br label %125

; <label>:163                                     ; preds = %125
  %164 = load i8* %error_copy_array, align 1
  %165 = trunc i8 %164 to i1
  br i1 %165, label %168, label %166

; <label>:166                                     ; preds = %163
  %167 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([22 x i8]* @.str8, i32 0, i32 0))
  br label %168

; <label>:168                                     ; preds = %166, %163
  %169 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 3, i64 1, i64 1, i64 3), align 4
  %170 = icmp eq i32 %169, 9
  br i1 %170, label %171, label %179

; <label>:171                                     ; preds = %168
  %172 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 3, i64 0, i64 0, i64 1), align 4
  %173 = icmp eq i32 %172, 100
  br i1 %173, label %174, label %179

; <label>:174                                     ; preds = %171
  %175 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 3, i64 0, i64 1, i64 1), align 4
  %176 = icmp eq i32 %175, 100
  br i1 %176, label %177, label %179

; <label>:177                                     ; preds = %174
  %178 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str9, i32 0, i32 0))
  br label %181

; <label>:179                                     ; preds = %174, %171, %168
  %180 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 85)
  br label %181

; <label>:181                                     ; preds = %179, %177
  %182 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 4, i64 1, i64 0, i64 0), align 4
  %183 = icmp eq i32 %182, 77
  br i1 %183, label %184, label %189

; <label>:184                                     ; preds = %181
  %185 = load i32* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 3, i64 1, i64 0, i64 0), align 4
  %186 = icmp ne i32 %185, 77
  br i1 %186, label %187, label %189

; <label>:187                                     ; preds = %184
  %188 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([21 x i8]* @.str10, i32 0, i32 0))
  br label %191

; <label>:189                                     ; preds = %184, %181
  %190 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 90)
  br label %191

; <label>:191                                     ; preds = %189, %187
  store i8 0, i8* %error_init_array_CartProduct, align 1
  store i32 0, i32* %i6, align 4
  br label %192

; <label>:192                                     ; preds = %215, %191
  %193 = load i32* %i6, align 4
  %194 = icmp sle i32 %193, 3
  br i1 %194, label %195, label %218

; <label>:195                                     ; preds = %192
  store i32 0, i32* %j7, align 4
  br label %196

; <label>:196                                     ; preds = %211, %195
  %197 = load i32* %j7, align 4
  %198 = icmp sle i32 %197, 3
  br i1 %198, label %199, label %214

; <label>:199                                     ; preds = %196
  %200 = load i32* %j7, align 4
  %201 = sext i32 %200 to i64
  %202 = load i32* %i6, align 4
  %203 = sext i32 %202 to i64
  %204 = getelementptr inbounds [4 x [4 x i32]]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 8), i32 0, i64 %203
  %205 = getelementptr inbounds [4 x i32]* %204, i32 0, i64 %201
  %206 = load i32* %205, align 4
  %207 = icmp ne i32 %206, 4
  br i1 %207, label %208, label %210

; <label>:208                                     ; preds = %199
  store i8 1, i8* %error_init_array_CartProduct, align 1
  %209 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 97)
  br label %210

; <label>:210                                     ; preds = %208, %199
  br label %211

; <label>:211                                     ; preds = %210
  %212 = load i32* %j7, align 4
  %213 = add nsw i32 %212, 1
  store i32 %213, i32* %j7, align 4
  br label %196

; <label>:214                                     ; preds = %196
  br label %215

; <label>:215                                     ; preds = %214
  %216 = load i32* %i6, align 4
  %217 = add nsw i32 %216, 1
  store i32 %217, i32* %i6, align 4
  br label %192

; <label>:218                                     ; preds = %192
  %219 = load i8* %error_init_array_CartProduct, align 1
  %220 = trunc i8 %219 to i1
  br i1 %220, label %223, label %221

; <label>:221                                     ; preds = %218
  %222 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([57 x i8]* @.str11, i32 0, i32 0))
  br label %223

; <label>:223                                     ; preds = %221, %218
  store i8 0, i8* %error_init_array_CartProduct, align 1
  store i32 0, i32* %i8, align 4
  br label %224

; <label>:224                                     ; preds = %272, %223
  %225 = load i32* %i8, align 4
  %226 = icmp sle i32 %225, 2
  br i1 %226, label %227, label %275

; <label>:227                                     ; preds = %224
  store i32 0, i32* %j9, align 4
  br label %228

; <label>:228                                     ; preds = %268, %227
  %229 = load i32* %j9, align 4
  %230 = icmp sle i32 %229, 3
  br i1 %230, label %231, label %271

; <label>:231                                     ; preds = %228
  store i32 0, i32* %k, align 4
  br label %232

; <label>:232                                     ; preds = %264, %231
  %233 = load i32* %k, align 4
  %234 = icmp sle i32 %233, 4
  br i1 %234, label %235, label %267

; <label>:235                                     ; preds = %232
  %236 = load i32* %k, align 4
  %237 = sext i32 %236 to i64
  %238 = load i32* %j9, align 4
  %239 = sext i32 %238 to i64
  %240 = load i32* %i8, align 4
  %241 = sext i32 %240 to i64
  %242 = getelementptr inbounds [11 x [21 x [31 x i32]]]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 9), i32 0, i64 %241
  %243 = getelementptr inbounds [21 x [31 x i32]]* %242, i32 0, i64 %239
  %244 = getelementptr inbounds [31 x i32]* %243, i32 0, i64 %237
  %245 = load i32* %244, align 4
  %246 = icmp ne i32 %245, 10
  br i1 %246, label %247, label %263

; <label>:247                                     ; preds = %235
  store i8 1, i8* %error_init_array_CartProduct, align 1
  %248 = load i32* %i8, align 4
  %249 = load i32* %j9, align 4
  %250 = load i32* %k, align 4
  %251 = load i32* %k, align 4
  %252 = sext i32 %251 to i64
  %253 = load i32* %j9, align 4
  %254 = sext i32 %253 to i64
  %255 = load i32* %i8, align 4
  %256 = sext i32 %255 to i64
  %257 = getelementptr inbounds [11 x [21 x [31 x i32]]]* getelementptr inbounds (%"struct.array$state$"* @array, i32 0, i32 9), i32 0, i64 %256
  %258 = getelementptr inbounds [21 x [31 x i32]]* %257, i32 0, i64 %254
  %259 = getelementptr inbounds [31 x i32]* %258, i32 0, i64 %252
  %260 = load i32* %259, align 4
  %261 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([16 x i8]* @.str12, i32 0, i32 0), i32 %248, i32 %249, i32 %250, i32 %260)
  %262 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str3, i32 0, i32 0), i32 111)
  br label %263

; <label>:263                                     ; preds = %247, %235
  br label %264

; <label>:264                                     ; preds = %263
  %265 = load i32* %k, align 4
  %266 = add nsw i32 %265, 1
  store i32 %266, i32* %k, align 4
  br label %232

; <label>:267                                     ; preds = %232
  br label %268

; <label>:268                                     ; preds = %267
  %269 = load i32* %j9, align 4
  %270 = add nsw i32 %269, 1
  store i32 %270, i32* %j9, align 4
  br label %228

; <label>:271                                     ; preds = %228
  br label %272

; <label>:272                                     ; preds = %271
  %273 = load i32* %i8, align 4
  %274 = add nsw i32 %273, 1
  store i32 %274, i32* %i8, align 4
  br label %224

; <label>:275                                     ; preds = %224
  %276 = load i8* %error_init_array_CartProduct, align 1
  %277 = trunc i8 %276 to i1
  br i1 %277, label %280, label %278

; <label>:278                                     ; preds = %275
  %279 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([68 x i8]* @.str13, i32 0, i32 0))
  br label %280

; <label>:280                                     ; preds = %278, %275
  %281 = load i32* %1
  ret i32 %281
}

declare void @"array$init$"(%"struct.array$state$"*) #1

declare i32 @printf(i8*, ...) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.53) (based on LLVM 3.6.0svn)"}

; ModuleID = 'initc-bublle.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

%"struct.bubble$state$" = type { [100 x i32], i32 }

@.str = private unnamed_addr constant [10 x i8] c"Original\0A\00", align 1
@bubble = common global %"struct.bubble$state$" zeroinitializer, align 4
@.str1 = private unnamed_addr constant [15 x i8] c" vec1[%d]= %d\0A\00", align 1
@.str2 = private unnamed_addr constant [12 x i8] c"After init\0A\00", align 1
@.str3 = private unnamed_addr constant [6 x i8] c" ...\0A\00", align 1
@.str4 = private unnamed_addr constant [12 x i8] c"After sort\0A\00", align 1

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  %i3 = alloca i32, align 4
  store i32 0, i32* %1
  %2 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([10 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %i1, align 4
  br label %3

; <label>:3                                       ; preds = %18, %0
  %4 = load i32* %i1, align 4
  %5 = icmp slt i32 %4, 100
  br i1 %5, label %6, label %21

; <label>:6                                       ; preds = %3
  %7 = load i32* %i1, align 4
  %8 = sub nsw i32 100, %7
  %9 = load i32* %i1, align 4
  %10 = sext i32 %9 to i64
  %11 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %10
  store i32 %8, i32* %11, align 4
  %12 = load i32* %i1, align 4
  %13 = load i32* %i1, align 4
  %14 = sext i32 %13 to i64
  %15 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %14
  %16 = load i32* %15, align 4
  %17 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str1, i32 0, i32 0), i32 %12, i32 %16)
  br label %18

; <label>:18                                      ; preds = %6
  %19 = load i32* %i1, align 4
  %20 = add nsw i32 %19, 1
  store i32 %20, i32* %i1, align 4
  br label %3

; <label>:21                                      ; preds = %3
  call void @"bubble$init$"(%"struct.bubble$state$"* @bubble)
  %22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str2, i32 0, i32 0))
  store i32 0, i32* %i2, align 4
  br label %23

; <label>:23                                      ; preds = %33, %21
  %24 = load i32* %i2, align 4
  %25 = icmp sle i32 %24, 5
  br i1 %25, label %26, label %36

; <label>:26                                      ; preds = %23
  %27 = load i32* %i2, align 4
  %28 = load i32* %i2, align 4
  %29 = sext i32 %28 to i64
  %30 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %29
  %31 = load i32* %30, align 4
  %32 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str1, i32 0, i32 0), i32 %27, i32 %31)
  br label %33

; <label>:33                                      ; preds = %26
  %34 = load i32* %i2, align 4
  %35 = add nsw i32 %34, 1
  store i32 %35, i32* %i2, align 4
  br label %23

; <label>:36                                      ; preds = %23
  %37 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([6 x i8]* @.str3, i32 0, i32 0))
  call void @"bubble$op_sort"(%"struct.bubble$state$"* @bubble)
  %38 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([12 x i8]* @.str4, i32 0, i32 0))
  store i32 0, i32* %i3, align 4
  br label %39

; <label>:39                                      ; preds = %49, %36
  %40 = load i32* %i3, align 4
  %41 = icmp slt i32 %40, 100
  br i1 %41, label %42, label %52

; <label>:42                                      ; preds = %39
  %43 = load i32* %i3, align 4
  %44 = load i32* %i3, align 4
  %45 = sext i32 %44 to i64
  %46 = getelementptr inbounds [100 x i32]* getelementptr inbounds (%"struct.bubble$state$"* @bubble, i32 0, i32 0), i32 0, i64 %45
  %47 = load i32* %46, align 4
  %48 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([15 x i8]* @.str1, i32 0, i32 0), i32 %43, i32 %47)
  br label %49

; <label>:49                                      ; preds = %42
  %50 = load i32* %i3, align 4
  %51 = add nsw i32 %50, 1
  store i32 %51, i32* %i3, align 4
  br label %39

; <label>:52                                      ; preds = %39
  %53 = load i32* %1
  ret i32 %53
}

declare i32 @printf(i8*, ...) #1

declare void @"bubble$init$"(%"struct.bubble$state$"*) #1

declare void @"bubble$op_sort"(%"struct.bubble$state$"*) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.53) (based on LLVM 3.6.0svn)"}

; ModuleID = 'rec_example_i.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

%struct.R_3 = type { i32 }
%struct.R_2 = type { i32, %struct.R_1 }
%struct.R_1 = type { i32, i32 }

@rec_example__bank = internal global %struct.R_3 zeroinitializer, align 4
@rec_example__client = internal global %struct.R_2 zeroinitializer, align 4

; Function Attrs: nounwind ssp uwtable
define void @rec_example__INITIALISATION() #0 {
  store i32 1, i32* getelementptr inbounds (%struct.R_3* @rec_example__bank, i32 0, i32 0), align 4
  store i32 0, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 0), align 4
  store i32 0, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  store i32 0, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 1), align 4
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @rec_example__positive_checking_account(i8* %res) #0 {
  %1 = alloca i8*, align 8
  store i8* %res, i8** %1, align 8
  %2 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 1), align 4
  %3 = icmp sgt i32 %2, 0
  br i1 %3, label %4, label %6

; <label>:4                                       ; preds = %0
  %5 = load i8** %1, align 8
  store i8 1, i8* %5, align 1
  br label %8

; <label>:6                                       ; preds = %0
  %7 = load i8** %1, align 8
  store i8 0, i8* %7, align 1
  br label %8

; <label>:8                                       ; preds = %6, %4
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @rec_example__withdraw(i32 %amt) #0 {
  %1 = alloca i32, align 4
  %new_balance = alloca i32, align 4
  store i32 %amt, i32* %1, align 4
  %2 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  %3 = load i32* %1, align 4
  %4 = sub nsw i32 %2, %3
  store i32 %4, i32* %new_balance, align 4
  %5 = load i32* %new_balance, align 4
  %6 = icmp sge i32 %5, -2147483647
  br i1 %6, label %7, label %9

; <label>:7                                       ; preds = %0
  %8 = load i32* %new_balance, align 4
  store i32 %8, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  br label %9

; <label>:9                                       ; preds = %7, %0
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @rec_example__deposit(i32 %amt) #0 {
  %1 = alloca i32, align 4
  %new_balance = alloca i32, align 4
  store i32 %amt, i32* %1, align 4
  %2 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  %3 = load i32* %1, align 4
  %4 = add nsw i32 %2, %3
  store i32 %4, i32* %new_balance, align 4
  %5 = load i32* %new_balance, align 4
  %6 = icmp sle i32 %5, 2147483647
  br i1 %6, label %7, label %9

; <label>:7                                       ; preds = %0
  %8 = load i32* %new_balance, align 4
  store i32 %8, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  br label %9

; <label>:9                                       ; preds = %7, %0
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @rec_example__swap() #0 {
  %1 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  %2 = icmp sge i32 %1, 0
  br i1 %2, label %3, label %10

; <label>:3                                       ; preds = %0
  %4 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  %5 = icmp sle i32 %4, 2147483647
  br i1 %5, label %6, label %10

; <label>:6                                       ; preds = %3
  %7 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 0), align 4
  store i32 %7, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 0), align 4
  %8 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 1), align 4
  store i32 %8, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  %9 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 0), align 4
  store i32 %9, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 1), align 4
  br label %10

; <label>:10                                      ; preds = %6, %3, %0
  ret void
}

; Function Attrs: nounwind ssp uwtable
define void @rec_example__unsafe_dec() #0 {
  %1 = load i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 1), align 4
  %2 = sub nsw i32 %1, 1
  store i32 %2, i32* getelementptr inbounds (%struct.R_2* @rec_example__client, i32 0, i32 1, i32 1), align 4
  ret void
}

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.49) (based on LLVM 3.6.0svn)"}

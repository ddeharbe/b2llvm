; ModuleID = 'declareArray.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.10.0"

@array = global [3 x [3 x [4 x i32]]] [[3 x [4 x i32]] [[4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5]], [3 x [4 x i32]] [[4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5]], [3 x [4 x i32]] [[4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5], [4 x i32] [i32 5, i32 5, i32 5, i32 5]]], align 16
@array2 = global [3 x [3 x [4 x i32]]] [[3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]], [3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]], [3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]]], align 16
@main.larray3 = private unnamed_addr constant [3 x [3 x [4 x i32]]] [[3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]], [3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]], [3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]]], align 16
@main.larray4 = private unnamed_addr constant [3 x [3 x [4 x i32]]] [[3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]], [3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]], [3 x [4 x i32]] [[4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10], [4 x i32] [i32 10, i32 10, i32 10, i32 10]]], align 16

; Function Attrs: nounwind ssp uwtable
define i32 @main() #0 {
  %larray3 = alloca [3 x [3 x [4 x i32]]], align 16
  %larray4 = alloca [3 x [3 x [4 x i32]]], align 16
  %1 = bitcast [3 x [3 x [4 x i32]]]* %larray3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %1, i8* bitcast ([3 x [3 x [4 x i32]]]* @main.larray3 to i8*), i64 144, i32 16, i1 false)
  %2 = bitcast [3 x [3 x [4 x i32]]]* %larray4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %2, i8* bitcast ([3 x [3 x [4 x i32]]]* @main.larray4 to i8*), i64 144, i32 16, i1 false)
  %3 = load i32* getelementptr inbounds ([3 x [3 x [4 x i32]]]* @array2, i32 0, i64 2, i64 1, i64 0), align 4
  store i32 %3, i32* getelementptr inbounds ([3 x [3 x [4 x i32]]]* @array, i32 0, i64 0, i64 1, i64 2), align 4
  ret i32 0
}

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #1

attributes #0 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"Apple LLVM version 6.1.0 (clang-602.0.49) (based on LLVM 3.6.0svn)"}

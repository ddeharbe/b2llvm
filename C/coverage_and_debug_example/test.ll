target triple = "x86_64-unknown-linux-gnu"

define i8 @foo(i32 %condi, i32 %a, i32 %b) !dbg !8 {
ent:
  %cond = icmp ne i32 %condi, 0, !dbg !13
  br i1 %cond, label %then, label %else, !dbg !14

then:
  %add = add i32 %a, %b, !dbg !15
  br label %ret, !dbg !16

else:
  %sub = sub i32 %a, %b, !dbg !17
  br label %ret, !dbg !18

ret:
  %phi = phi i32 [%add, %then], [%sub, %else], !dbg !19
  %tr = trunc i32 %phi to i8, !dbg !20
  ret i8 %tr, !dbg !21
}

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}
!llvm.dbg.cu = !{!3}
!llvm.gcov = !{!4}

!0 = !{i32 2, !"Dwarf Version", i32 2}
!1 = !{i32 2, !"Debug Info Version", i32 3}
!2 = !{!"clang version 3.9.0"}
!3 = distinct !DICompileUnit(
    language: DW_LANG_C99, file: !5, producer: "clang version 3.9.0",
    isOptimized: false, emissionKind: LineTablesOnly, enums: !6,
    runtimeVersion: 0, imports: !6)
!4 = !{!"test.ll", !3}
!5 = !DIFile(filename: "test.ll", directory: "")
!6 = !{}
!7 = !{!8}
!8 = distinct !DISubprogram(
    name: "foo", line: 3, isLocal: false, isDefinition: true, isOptimized: false,
    scopeLine: 3, file: !5, scope: !5, type: !12, variables: !6, unit: !3)
!9 = !DIBasicType(name: "char", size: 8, align: 8, encoding: DW_ATE_signed_char)
!10 = !DIBasicType(name: "int", size: 32, align: 32, encoding: DW_ATE_signed)
!11 = !{!9, !10, !10, !10}
!12 = !DISubroutineType(types: !11)
!13 = !DILocation(line: 5, column: 2, scope: !8)
!14 = !DILocation(line: 6, column: 2, scope: !8)
!15 = !DILocation(line: 9, column: 2, scope: !8)
!16 = !DILocation(line: 10, column: 2, scope: !8)
!17 = !DILocation(line: 13, column: 2, scope: !8)
!18 = !DILocation(line: 14, column: 2, scope: !8)
!19 = !DILocation(line: 17, column: 2, scope: !8)
!20 = !DILocation(line: 18, column: 2, scope: !8)
!21 = !DILocation(line: 19, column: 2, scope: !8)

; ModuleID = 'form.c'
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.11.0"

; Function Attrs: nounwind readnone ssp uwtable
define zeroext i1 @f1(i1 zeroext %a, i1 zeroext %b) #0 {
  ret i1 true
}

; Function Attrs: nounwind readnone ssp uwtable
define zeroext i1 @f2(i1 zeroext %a, i1 zeroext %b) #0 {
  %1 = xor i1 %a, true
  ret i1 %1
}

; Function Attrs: nounwind readnone ssp uwtable
define zeroext i1 @f3(i1 zeroext %ta, i1 zeroext %tb, i1 zeroext %tc) #0 {
  %brmerge = or i1 %ta, %tb
  %tc. = and i1 %brmerge, %tc
  ret i1 %tc.
}

attributes #0 = { nounwind readnone ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="core2" "target-features"="+ssse3,+cx16,+sse,+sse2,+sse3" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"PIC Level", i32 2}
!1 = !{!"Apple LLVM version 7.0.0 (clang-700.1.76)"}

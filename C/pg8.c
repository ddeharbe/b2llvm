#include <stdio.h>
typedef struct {
  int x;
  int y;
} * Tpoint;

typedef struct {
  int x;
  int y;
} ET;

void ts(struct { int x; int y; }* t){
  t->x=88;
}


int main(){
  ET a;
  ET * b;
  b = &a;
  a.x=30;
  b->x = 10;
  printf("%d\n",b->x);
  ts(b);
  printf("%d\n",b->x);
}

void ds (Tpoint m, float d)
{
  m->x = d;
}

void dx (Tpoint m, float d)
{
  m->x += d;
}

typedef struct {
  Tpoint p1;
  Tpoint p2;
} * Tsegment;

void segment_dx(Tsegment s, float d)
{
  dx(s->p1, d);
  dx(s->p2, d);
}

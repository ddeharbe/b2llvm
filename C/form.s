	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_f1
	.align	4, 0x90
_f1:                                    ## @f1
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	movb	$1, %al
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_f2
	.align	4, 0x90
_f2:                                    ## @f2
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	andb	%sil, %dil
	movb	%dil, %al
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_f3
	.align	4, 0x90
_f3:                                    ## @f3
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	orb	%sil, %dil
	andb	%dil, %dl
	movb	%dl, %al
	popq	%rbp
	retq
	.cfi_endproc


.subsections_via_symbols

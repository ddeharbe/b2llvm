#include <stdbool.h>

bool f1(bool a, bool b)
{
  bool c;
  c = true;
  return c;
}
bool f2(bool a, bool b)
{
  bool c;
  c = !(a);
  //c = a && b;
  return c; //return a && b || c;
}
bool f3(bool ta, bool tb, bool tc )
{
  bool c;
  //c = a == true ? b : false;
  //c = !(true);
  c = (ta || tb) && tc;
  //return a && b || c;
  return c;
}
